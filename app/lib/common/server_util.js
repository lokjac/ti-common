var socket 								= null;
var connectedSockets 					= [];
var socket_receive_request_callback 	= null;
//var socket_response_callback 			= null;

function removeSocket(sock) {
	var index = connectedSockets.indexOf(sock);
	if (index != -1) {
		connectedSockets.splice(index,1);
	}	
}

function pumpCallback(e) {
	Ti.API.debug('---pumpCallback---');
	if (e.bytesProcessed == -1) { // EOF
		Ti.API.debug('<EOF> - Closing the remote socket!');
		removeSocket(e.source);
		e.source.close();
	}else 
	if (e.errorDescription == null || e.errorDescription == "") {
		Ti.API.debug("DATA: "+e.buffer.toString());
		var request = e.buffer.toString();
		if(socket_receive_request_callback){
			var response = socket_receive_request_callback(request);
			if(response){
				var sock = e.source;
				sock.write(Ti.createBuffer({
			            value : response
			    }));
			}
		}
		
	}else {
		Ti.API.error("READ ERROR: "+e.errorDescription);
		
	}
}

var acceptedCallbacks = {
	error : function(e) {
		Ti.UI.createAlertDialog({
			title:"Server socket error: "+e.socket.host,
			message:e.error
		}).show();
		removeSocket(e.socket);
	}
};

function acceptedSocketConnection(e) {
	var sock = e.inbound;
	connectedSockets.push(sock);
	Ti.API.debug('ACCEPTED: '+sock.host+':'+sock.port);
	Ti.Stream.pump(sock, pumpCallback, 1024, true);
	
	socket.accept(acceptedCallbacks);
}

function closeSocketConnection(e){
	Ti.API.debug('closeSocketConnection: Closed socket');
}

function errorOnSocketConnection(e){
	Ti.API.debug('errorOnSocketConnection: Error on socket');
	Ti.UI.createAlertDialog({
		title	: "Listener error: "+e.errorCode,
		message	: e.error
	}).show();
}

function createSocket(hostname, port_no, listen_queue_size, receive_request_callback){
	Ti.API.debug('socket='+socket);
	Ti.API.debug('Ti.Network.Socket.CLOSED='+Ti.Network.Socket.CLOSED);
	Ti.API.debug('Ti.Network.Socket.ERROR='+Ti.Network.Socket.ERROR);
	Ti.API.debug('Ti.Network.Socket.INITIALIZED='+Ti.Network.Socket.INITIALIZED);
	
	socket_receive_request_callback = receive_request_callback;
	//socket_response_callback		= response_callback;
	
	if (socket == null || socket.state==Ti.Network.Socket.CLOSED) {
		//var _hostname = Ti.Platform.address;//(Ti.Platform.username == 'iPhone Simulator' ? hostname : Ti.Platform.address);
		var _hostname 	= hostname!=null && hostname!='' ? hostname : Ti.Platform.address;
		var _port_no 	= port_no!=null && port_no!=''!=null ? port_no : 1234;
		//var _hostname = hostname!=null ? hostname : Ti.Platform.address;
		socket = Ti.Network.Socket.createTCP({
			host			: _hostname,
			port			: _port_no,
			listenQueueSize	: (listen_queue_size==null? 100: listen_queue_size),
			accepted		: acceptedSocketConnection,
			closed			: closeSocketConnection,
			error			: errorOnSocketConnection,
			
		});
		
		Ti.API.debug('_hostname='+_hostname);
		Ti.API.debug('socket='+socket);
		Ti.API.debug('Created socket');
	}
	
	try {
			socket.listen();
			Ti.API.debug('Already started to listen');
			//Ti.API.debug("Listening on "+socket.host+":"+socket.port);
			
			// Tells socket to accept the next inbound connection. listenSocket.accepted gets
			// called when a connection is accepted via accept()
			Ti.API.info("Calling accept.");
			socket.accept({
			    timeout : 10000
			});
			
			//socket.accept(acceptedCallbacks);
	} catch (e) {
		socket = null;
		Ti.API.error('createSocket: Exception: '+e);
	}
	
	return socket;
}

function writeRequest(request_input, writeCallback){
	Ti.API.debug('request_input='+request_input);
	
	Ti.Stream.write(socket, Ti.createBuffer({
            value: request_input
        }), function(e){
        	if(writeCallback){
        		writeCallback(e);
        	}
        });
}

function writeRequestBuffer(request_input_buffer, writeCallback){
	
	Ti.Stream.write(socket, request_input_buffer, function(e){
        	if(writeCallback){
        		writeCallback(e);
        	}
        });
}

function closeSocket(){
	try {
		if(socket){
			socket.close();
		
			for (var index in connectedSockets) {
				try {
					var sock = connectedSockets[index];
					sock.close();
				}catch (e) {
					Ti.API.error("Exception: " +e);
					
				}
			}
			Ti.API.debug('Closed socket');
			socket = null;
		}else{
			Ti.API.debug('No socket is created');
		}
	} catch (e) {
		Ti.API.error("Exception: " +e);
	}
}

function checkSocketStatus(){
	var stateString = "UNKNOWN";
	if (socket != null) {
		switch (socket.state) {
			case Ti.Network.Socket.INITIALIZED:
				stateString = "INITIALIZED";
				break;
			case Ti.Network.Socket.CONNECTED:
				stateString = "CONNECTED";
				break;
			case Ti.Network.Socket.LISTENING:
				stateString = "LISTENING";
				break;
			case Ti.Network.Socket.CLOSED:
				stateString = "CLOSED";
				break;
			case Ti.Network.Socket.ERROR:
				stateString = "ERROR";
				break;
		}
		return socket.host+": "+socket.port+" "+stateString;
	}else{
		return 'No socket is created';
	}
	
}

function connectSocket(hostname, port, connectedCallback){
	socket = Ti.Network.Socket.createTCP({
	    host : hostname,
	    port : port,
	    connected : function(e) {
	        Ti.API.info('Client socket connected!');
	        //Ti.Stream.pump(e.socket, pumpCallback, 1024, true);
	        /*
	        e.socket.write(Ti.createBuffer({
	            value : 'A message from a connecting socket.'
	        }));
	        */
	       if(connectedCallback){
	       	connectedCallback();
	       }
	    },
	    error : function(e) {
	        Ti.API.info('Error (' + e.errorCode + '): ' + e.error);
	    }
	});
	setTimeout(function(e)
	{
	    Ti.API.info("Calling connect on client socket.");
	    socket.connect();
	}, 500);
}

exports.createSocket			= createSocket;
exports.connectSocket			= connectSocket;
exports.checkSocketStatus		= checkSocketStatus;
exports.closeSocket				= closeSocket;
exports.writeRequest			= writeRequest;
exports.writeRequestBuffer		= writeRequestBuffer;




