var cnty_data_source = [
    {
      "cnty_name": "Afghanistan",
      "phone_code": "+93",
      "trunk_code": "0",
      "cnty_code": "af"
    },
    {
      "cnty_name": "Albania",
      "phone_code": "+355",
      "trunk_code": "0",
      "cnty_code": "al"
    },
    {
      "cnty_name": "Algeria",
      "phone_code": "+213",
      "trunk_code": "0",
      "cnty_code": "dz"
    },
    {
      "cnty_name": "American Samoa",
      "phone_code": "+1684",
      "trunk_code": "1",
      "cnty_code": "as"
    },
    {
      "cnty_name": "Andorra",
      "phone_code": "+376",
      "cnty_code": "ad"
    },
    {
      "cnty_name": "Angola",
      "phone_code": "+244",
      "cnty_code": "ao"
    },
    {
      "cnty_name": "Anguilla",
      "phone_code": "+1264",
      "trunk_code": "1",
      "cnty_code": "ai"
    },
    {
      "cnty_name": "Antigua and Barbuda",
      "phone_code": "+1264",
      "trunk_code": "1",
      "cnty_code": "ag"
    },
    {
      "cnty_name": "Argentina",
      "phone_code": "+54",
      "trunk_code": "0",
      "cnty_code": "ar"
    },
    {
      "cnty_name": "Armenia",
      "phone_code": "+374",
      "trunk_code": "0",
      "cnty_code": "am"
    },
    {
      "cnty_name": "Aruba",
      "phone_code": "+297",
      "cnty_code": "aw"
    },
    {
      "cnty_name": "Australia",
      "phone_code": "+61",
      "trunk_code": "0",
      "cnty_code": "au"
    },
    {
      "cnty_name": "Austria",
      "phone_code": "+43",
      "trunk_code": "0",
      "cnty_code": "at"
    },
    {
      "cnty_name": "Azerbaijan",
      "phone_code": "+994",
      "trunk_code": "0",
      "cnty_code": "az"
    },
    {
      "cnty_name": "Bahamas",
      "phone_code": "+1242",
      "trunk_code": "1",
      "cnty_code": "bs"
    },
    {
      "cnty_name": "Bahrain",
      "phone_code": "+973",
      "cnty_code": "bh"
    },
    {
      "cnty_name": "Bangladesh",
      "phone_code": "+880",
      "trunk_code": "0",
      "cnty_code": "bd"
    },
    {
      "cnty_name": "Barbados",
      "phone_code": "+1246",
      "trunk_code": "1",
      "cnty_code": "bb"
    },
    {
      "cnty_name": "Belarus",
      "phone_code": "+375",
      "trunk_code": "80",
      "cnty_code": "by"
    },
    {
      "cnty_name": "Belgium",
      "phone_code": "+32",
      "trunk_code": "0",
      "cnty_code": "be"
    },
    {
      "cnty_name": "Belize",
      "phone_code": "+501",
      "cnty_code": "bz"
    },
    {
      "cnty_name": "Benin",
      "phone_code": "+229",
      "cnty_code": "bj"
    },
    {
      "cnty_name": "Bermuda",
      "phone_code": "+1441",
      "trunk_code": "1",
      "cnty_code": "bm"
    },
    {
      "cnty_name": "Bhutan",
      "phone_code": "+975",
      "cnty_code": "bt"
    },
    {
      "cnty_name": "Bolivia",
      "phone_code": "+591",
      "trunk_code": "0",
      "cnty_code": "bo"
    },
    {
      "cnty_name": "Bosnia and Herzegovina",
      "phone_code": "+387",
      "trunk_code": "0",
      "cnty_code": "ba"
    },
    {
      "cnty_name": "Botswana",
      "phone_code": "+267",
      "cnty_code": "bw"
    },
    {
      "cnty_name": "Brazil",
      "phone_code": "+55",
      "trunk_code": "0",
      "cnty_code": "br"
    },
    {
      "cnty_name": "Brunei",
      "phone_code": "+673",
      "cnty_code": "bn"
    },
    {
      "cnty_name": "Bulgaria",
      "phone_code": "+359",
      "trunk_code": "0",
      "cnty_code": "bg"
    },
    {
      "cnty_name": "Burkina Faso",
      "phone_code": "+226",
      "cnty_code": "bf"
    },
    {
      "cnty_name": "Burundi",
      "phone_code": "+257",
      "cnty_code": "bi"
    },
    {
      "cnty_name": "Cambodia",
      "phone_code": "+855",
      "trunk_code": "0",
      "cnty_code": "kh"
    },
    {
      "cnty_name": "Cameroon",
      "phone_code": "+237",
      "cnty_code": "cm"
    },
    {
      "cnty_name": "Canada",
      "phone_code": "+1",
      "trunk_code": "1",
      "cnty_code": "ca"
    },
    {
      "cnty_name": "Cape Verde",
      "phone_code": "+238",
      "cnty_code": "cv"
    },
    {
      "cnty_name": "Cayman Islands",
      "phone_code": "+1345",
      "trunk_code": "1",
      "cnty_code": "ky"
    },
    {
      "cnty_name": "Central African Republic",
      "phone_code": "+236",
      "cnty_code": "cf"
    },
    {
      "cnty_name": "Chad",
      "phone_code": "+235",
      "cnty_code": "td"
    },
    {
      "cnty_name": "Chile",
      "phone_code": "+56",
      "trunk_code": "0",
      "cnty_code": "cl"
    },
    {
      "cnty_name": "China",
      "phone_code": "+86",
      "trunk_code": "0",
      "cnty_code": "cn"
    },
    {
      "cnty_name": "Colombia",
      "phone_code": "+57",
      "trunk_code": "0",
      "cnty_code": "co"
    },
    {
      "cnty_name": "Comoros",
      "phone_code": "+269",
      "cnty_code": "km"
    },
    {
      "cnty_name": "Cook Islands",
      "phone_code": "+682",
      "cnty_code": "ck"
    },
    {
      "cnty_name": "Costa Rica",
      "phone_code": "+506",
      "cnty_code": "cr"
    },
    {
      "cnty_name": "Croatia",
      "phone_code": "+385",
      "trunk_code": "0",
      "cnty_code": "hr"
    },
    {
      "cnty_name": "Cuba",
      "phone_code": "+53",
      "trunk_code": "0",
      "cnty_code": "cu"
    },
    {
      "cnty_name": "Cyprus",
      "phone_code": "+357",
      "cnty_code": "cy"
    },
    {
      "cnty_name": "Czech Republic",
      "phone_code": "+420",
      "cnty_code": "cz"
    },
    {
      "cnty_name": "Democratic Republic of the Congo",
      "phone_code": "+243",
      "trunk_code": "0",
      "cnty_code": "cd"
    },
    {
      "cnty_name": "Denmark",
      "phone_code": "+45",
      "cnty_code": "dk"
    },
    {
      "cnty_name": "Djibouti",
      "phone_code": "+253",
      "cnty_code": "dj"
    },
    {
      "cnty_name": "Dominica",
      "phone_code": "+1767",
      "trunk_code": "1",
      "cnty_code": "dm"
    },
    {
      "cnty_name": "Ecuador",
      "phone_code": "+593",
      "trunk_code": "0",
      "cnty_code": "ec"
    },
    {
      "cnty_name": "Egypt",
      "phone_code": "+20",
      "trunk_code": "0",
      "cnty_code": "eg"
    },
    {
      "cnty_name": "El Salvador",
      "phone_code": "+503",
      "cnty_code": "sv"
    },
    {
      "cnty_name": "Equatorial Guinea",
      "phone_code": "+240",
      "cnty_code": "gq"
    },
    {
      "cnty_name": "Eritrea",
      "phone_code": "+291",
      "trunk_code": "0",
      "cnty_code": "er"
    },
    {
      "cnty_name": "Estonia",
      "phone_code": "+372",
      "cnty_code": "ee"
    },
    {
      "cnty_name": "Ethiopia",
      "phone_code": "+251",
      "trunk_code": "0",
      "cnty_code": "et"
    },
    {
      "cnty_name": "Falkland Islands (Islas Malvinas)",
      "phone_code": "+500",
      "cnty_code": "fk"
    },
    {
      "cnty_name": "Faroe Islands",
      "phone_code": "+298",
      "cnty_code": "fo"
    },
    {
      "cnty_name": "Fiji",
      "phone_code": "+679",
      "cnty_code": "fj"
    },
    {
      "cnty_name": "Finland",
      "phone_code": "+358",
      "trunk_code": "0",
      "cnty_code": "fi"
    },
    {
      "cnty_name": "France",
      "phone_code": "+33",
      "trunk_code": "0",
      "cnty_code": "fr"
    },
    {
      "cnty_name": "Gabon",
      "phone_code": "+241",
      "cnty_code": "ga"
    },
    {
      "cnty_name": "Gambia",
      "phone_code": "+220",
      "cnty_code": "gm"
    },
    {
      "cnty_name": "Georgia",
      "phone_code": "+995",
      "trunk_code": "0",
      "cnty_code": "ge"
    },
    {
      "cnty_name": "Germany",
      "phone_code": "+49",
      "trunk_code": "0",
      "cnty_code": "de"
    },
    {
      "cnty_name": "Ghana",
      "phone_code": "+233",
      "trunk_code": "0",
      "cnty_code": "gh"
    },
    {
      "cnty_name": "Gibraltar",
      "phone_code": "+350",
      "cnty_code": "gi"
    },
    {
      "cnty_name": "Greece",
      "phone_code": "+30",
      "cnty_code": "gr"
    },
    {
      "cnty_name": "Greenland",
      "phone_code": "+299",
      "cnty_code": "gl"
    },
    {
      "cnty_name": "Grenada",
      "phone_code": "+1473",
      "trunk_code": "1",
      "cnty_code": "gd"
    },
    {
      "cnty_name": "Guam",
      "phone_code": "+1671",
      "trunk_code": "1",
      "cnty_code": "gu"
    },
    {
      "cnty_name": "Guatemala",
      "phone_code": "+502",
      "cnty_code": "gt"
    },
    {
      "cnty_name": "Guernsey",
      "phone_code": "+441481",
      "trunk_code": "0",
      "cnty_code": "gg"
    },
    {
      "cnty_name": "Guinea",
      "phone_code": "+224",
      "cnty_code": "gn"
    },
    {
      "cnty_name": "Guinea-Bissau",
      "phone_code": "+245",
      "cnty_code": "gw"
    },
    {
      "cnty_name": "Guyana",
      "phone_code": "+592",
      "cnty_code": "gy"
    },
    {
      "cnty_name": "Haiti",
      "phone_code": "+509",
      "cnty_code": "ht"
    },
    {
      "cnty_name": "Honduras",
      "phone_code": "+504",
      "cnty_code": "hn"
    },
    {
      "cnty_name": "Hong Kong",
      "phone_code": "+852",
      "cnty_code": "hk"
    },
    {
      "cnty_name": "Hungary",
      "phone_code": "+36",
      "trunk_code": "06",
      "cnty_code": "hu"
    },
    {
      "cnty_name": "Iceland",
      "phone_code": "+354",
      "cnty_code": "is"
    },
    {
      "cnty_name": "India",
      "phone_code": "+91",
      "trunk_code": "0",
      "cnty_code": "in"
    },
    {
      "cnty_name": "Indonesia",
      "phone_code": "+62",
      "trunk_code": "0",
      "cnty_code": "id"
    },
    {
      "cnty_name": "Iran",
      "phone_code": "+98",
      "trunk_code": "0",
      "cnty_code": "ir"
    },
    {
      "cnty_name": "Iraq",
      "phone_code": "+964",
      "cnty_code": "iq"
    },
    {
      "cnty_name": "Ireland",
      "phone_code": "+353",
      "trunk_code": "0",
      "cnty_code": "ie"
    },
    {
      "cnty_name": "Isle of Man",
      "phone_code": "+441624",
      "trunk_code": "0",
      "cnty_code": "im"
    },
    {
      "cnty_name": "Israel",
      "phone_code": "+972",
      "trunk_code": "0",
      "cnty_code": "il"
    },
    {
      "cnty_name": "Italy",
      "phone_code": "+39",
      "cnty_code": "it"
    },
    {
      "cnty_name": "Jamaica",
      "phone_code": "+1876",
      "trunk_code": "1",
      "cnty_code": "jm"
    },
    {
      "cnty_name": "Japan",
      "phone_code": "+81",
      "trunk_code": "0",
      "cnty_code": "jp"
    },
    {
      "cnty_name": "Jersey",
      "phone_code": "+441534",
      "trunk_code": "1",
      "cnty_code": "je"
    },
    {
      "cnty_name": "Jordan",
      "phone_code": "+962",
      "trunk_code": "0",
      "cnty_code": "jo"
    },
    {
      "cnty_name": "Kazakhstan",
      "phone_code": "+7",
      "trunk_code": "8",
      "cnty_code": "kz"
    },
    {
      "cnty_name": "Kenya",
      "phone_code": "+254",
      "trunk_code": "0",
      "cnty_code": "ke"
    },
    {
      "cnty_name": "Kiribati",
      "phone_code": "+686",
      "cnty_code": "ki"
    },
    {
      "cnty_name": "Kuwait",
      "phone_code": "+965",
      "cnty_code": "kw"
    },
    {
      "cnty_name": "Kyrgyzstan",
      "phone_code": "+996",
      "cnty_code": "kg"
    },
    {
      "cnty_name": "Laos",
      "phone_code": "+856",
      "trunk_code": "0",
      "cnty_code": "la"
    },
    {
      "cnty_name": "Latvia",
      "phone_code": "+371",
      "cnty_code": "lv"
    },
    {
      "cnty_name": "Lebanon",
      "phone_code": "+961",
      "trunk_code": "0",
      "cnty_code": "lb"
    },
    {
      "cnty_name": "Lesotho",
      "phone_code": "+266",
      "cnty_code": "ls"
    },
    {
      "cnty_name": "Liberia",
      "phone_code": "+231",
      "cnty_code": "lr"
    },
    {
      "cnty_name": "Libya",
      "phone_code": "+218",
      "trunk_code": "0",
      "cnty_code": "ly"
    },
    {
      "cnty_name": "Liechtenstein",
      "phone_code": "+423",
      "cnty_code": "li"
    },
    {
      "cnty_name": "Lithuania",
      "phone_code": "+370",
      "trunk_code": "8",
      "cnty_code": "lt"
    },
    {
      "cnty_name": "Luxembourg",
      "phone_code": "+352",
      "cnty_code": "lu"
    },
    {
      "cnty_name": "Macedonia",
      "phone_code": "+389",
      "trunk_code": "0",
      "cnty_code": "mk"
    },
    {
      "cnty_name": "Madagascar",
      "phone_code": "+261",
      "trunk_code": "0",
      "cnty_code": "mg"
    },
    {
      "cnty_name": "Malawi",
      "phone_code": "+265",
      "cnty_code": "mw"
    },
    {
      "cnty_name": "Malaysia",
      "phone_code": "+60",
      "trunk_code": "0",
      "cnty_code": "my"
    },
    {
      "cnty_name": "Maldives",
      "phone_code": "+960",
      "cnty_code": "mv"
    },
    {
      "cnty_name": "Mali",
      "phone_code": "+223",
      "cnty_code": "ml"
    },
    {
      "cnty_name": "Malta",
      "phone_code": "+356",
      "cnty_code": "mt"
    },
    {
      "cnty_name": "Marshall Islands",
      "phone_code": "+692",
      "trunk_code": "1",
      "cnty_code": "mh"
    },
    {
      "cnty_name": "Mauritania",
      "phone_code": "+222",
      "cnty_code": "mr"
    },
    {
      "cnty_name": "Mauritius",
      "phone_code": "+230",
      "cnty_code": "mu"
    },
    {
      "cnty_name": "Mayotte",
      "phone_code": "+262",
      "trunk_code": "0",
      "cnty_code": "yt"
    },
    {
      "cnty_name": "Mexico",
      "phone_code": "+52",
      "trunk_code": "01,044,045",
      "cnty_code": "mx"
    },
    {
      "cnty_name": "Micronesia",
      "phone_code": "+691",
      "trunk_code": "1",
      "cnty_code": "fm"
    },
    {
      "cnty_name": "Moldova",
      "phone_code": "+373",
      "trunk_code": "0",
      "cnty_code": "md"
    },
    {
      "cnty_name": "Monaco",
      "phone_code": "+377",
      "cnty_code": "mc"
    },
    {
      "cnty_name": "Mongolia",
      "phone_code": "+976",
      "trunk_code": "0",
      "cnty_code": "mn"
    },
    {
      "cnty_name": "Montenegro",
      "phone_code": "+382",
      "trunk_code": "0",
      "cnty_code": "me"
    },
    {
      "cnty_name": "Montserrat",
      "phone_code": "+1664",
      "trunk_code": "1",
      "cnty_code": "ms"
    },
    {
      "cnty_name": "Morocco",
      "phone_code": "+377",
      "trunk_code": "0",
      "cnty_code": "ma"
    },
    {
      "cnty_name": "Mozambique",
      "phone_code": "+258",
      "cnty_code": "mz"
    },
    {
      "cnty_name": "Myanmar (Burma)",
      "phone_code": "+95",
      "trunk_code": "0",
      "cnty_code": "mm"
    },
    {
      "cnty_name": "Namibia",
      "phone_code": "+264",
      "trunk_code": "0",
      "cnty_code": "na"
    },
    {
      "cnty_name": "Nauru",
      "phone_code": "+674",
      "cnty_code": "nr"
    },
    {
      "cnty_name": "Nepal",
      "phone_code": "+977",
      "trunk_code": "0",
      "cnty_code": "np"
    },
    {
      "cnty_name": "Netherlands",
      "phone_code": "+31",
      "trunk_code": "0",
      "cnty_code": "nl"
    },
    {
      "cnty_name": "Netherlands Antilles",
      "phone_code": "+599",
      "trunk_code": "0",
      "cnty_code": "an"
    },
    {
      "cnty_name": "New Zealand",
      "phone_code": "+64",
      "trunk_code": "0",
      "cnty_code": "nz"
    },
    {
      "cnty_name": "Nicaragua",
      "phone_code": "+505",
      "cnty_code": "ni"
    },
    {
      "cnty_name": "Niger",
      "phone_code": "+227",
      "cnty_code": "ne"
    },
    {
      "cnty_name": "Nigeria",
      "phone_code": "+234",
      "trunk_code": "0",
      "cnty_code": "ng"
    },
    {
      "cnty_name": "Niue",
      "phone_code": "+683",
      "cnty_code": "nu"
    },
    {
      "cnty_name": "North Korea",
      "phone_code": "+850",
      "cnty_code": "kp"
    },
    {
      "cnty_name": "Norway",
      "phone_code": "+47",
      "cnty_code": "no"
    },
    {
      "cnty_name": "Oman",
      "phone_code": "+968",
      "cnty_code": "om"
    },
    {
      "cnty_name": "Pakistan",
      "phone_code": "+92",
      "trunk_code": "0",
      "cnty_code": "pk"
    },
    {
      "cnty_name": "Palau",
      "phone_code": "+680",
      "cnty_code": "pw"
    },
    {
      "cnty_name": "Panama",
      "phone_code": "+507",
      "cnty_code": "pa"
    },
    {
      "cnty_name": "Papua New Guinea",
      "phone_code": "+675",
      "cnty_code": "pg"
    },
    {
      "cnty_name": "Paraguay",
      "phone_code": "+595",
      "trunk_code": "0",
      "cnty_code": "py"
    },
    {
      "cnty_name": "Peru",
      "phone_code": "+51",
      "trunk_code": "0",
      "cnty_code": "pe"
    },
    {
      "cnty_name": "Philippines",
      "phone_code": "+63",
      "trunk_code": "0",
      "cnty_code": "ph"
    },
    {
      "cnty_name": "Poland",
      "phone_code": "+48",
      "cnty_code": "pl"
    },
    {
      "cnty_name": "Portugal",
      "phone_code": "+351",
      "cnty_code": "pt"
    },
    {
      "cnty_name": "Qatar",
      "phone_code": "+974",
      "cnty_code": "qa"
    },
    {
      "cnty_name": "Reunion",
      "phone_code": "+262",
      "trunk_code": "0",
      "cnty_code": "re"
    },
    {
      "cnty_name": "Romania",
      "phone_code": "+40",
      "trunk_code": "0",
      "cnty_code": "ro"
    },
    {
      "cnty_name": "Russia",
      "phone_code": "+7",
      "trunk_code": "8",
      "cnty_code": "ru"
    },
    {
      "cnty_name": "Rwanda",
      "phone_code": "+250",
      "cnty_code": "rw"
    },
    {
      "cnty_name": "Saint Helena",
      "phone_code": "+290",
      "cnty_code": "sh"
    },
    {
      "cnty_name": "Saint Kitts and Nevis",
      "phone_code": "+1869",
      "trunk_code": "1",
      "cnty_code": "kn"
    },
    {
      "cnty_name": "Saint Lucia",
      "phone_code": "+1758",
      "trunk_code": "1",
      "cnty_code": "lc"
    },
    {
      "cnty_name": "Saint Martin",
      "phone_code": "+590",
      "trunk_code": "0",
      "cnty_code": "gp"
    },
    {
      "cnty_name": "Saint Pierre and Miquelon",
      "phone_code": "+508",
      "cnty_code": "pm"
    },
    {
      "cnty_name": "Saint Vincent and the Grenadines",
      "phone_code": "+1784",
      "trunk_code": "1",
      "cnty_code": "vc"
    },
    {
      "cnty_name": "Samoa",
      "phone_code": "+685",
      "cnty_code": "ws"
    },
    {
      "cnty_name": "San Marino",
      "phone_code": "+378",
      "cnty_code": "sm"
    },
    {
      "cnty_name": "Sao Tome and Principe",
      "phone_code": "+239",
      "cnty_code": "st"
    },
    {
      "cnty_name": "Saudi Arabia",
      "phone_code": "+966",
      "trunk_code": "0",
      "cnty_code": "sa"
    },
    {
      "cnty_name": "Senegal",
      "phone_code": "+221",
      "cnty_code": "sn"
    },
    {
      "cnty_name": "Serbia",
      "phone_code": "+381",
      "trunk_code": "0",
      "cnty_code": "rs"
    },
    {
      "cnty_name": "Seychelles",
      "phone_code": "+248",
      "cnty_code": "sc"
    },
    {
      "cnty_name": "Sierra Leone",
      "phone_code": "+232",
      "trunk_code": "0",
      "cnty_code": "sl"
    },
    {
      "cnty_name": "Singapore",
      "phone_code": "+65",
      "cnty_code": "sg"
    },
    {
      "cnty_name": "Slovakia",
      "phone_code": "+421",
      "trunk_code": "0",
      "cnty_code": "sk"
    },
    {
      "cnty_name": "Slovenia",
      "phone_code": "+386",
      "trunk_code": "0",
      "cnty_code": "si"
    },
    {
      "cnty_name": "Solomon Islands",
      "phone_code": "+677",
      "cnty_code": "sb"
    },
    {
      "cnty_name": "Somalia",
      "phone_code": "+252",
      "cnty_code": "so"
    },
    {
      "cnty_name": "South Africa",
      "phone_code": "+27",
      "trunk_code": "0",
      "cnty_code": "za"
    },
    {
      "cnty_name": "South Korea",
      "phone_code": "+82",
      "trunk_code": "0",
      "cnty_code": "kr"
    },
    {
      "cnty_name": "Spain",
      "phone_code": "+34",
      "cnty_code": "es"
    },
    {
      "cnty_name": "Sri Lanka",
      "phone_code": "+94",
      "trunk_code": "0",
      "cnty_code": "lk"
    },
    {
      "cnty_name": "Sudan",
      "phone_code": "+249",
      "trunk_code": "0",
      "cnty_code": "sd"
    },
    {
      "cnty_name": "Suriname",
      "phone_code": "+597",
      "trunk_code": "0",
      "cnty_code": "sr"
    },
    {
      "cnty_name": "Svalbard",
      "phone_code": "+47",
      "cnty_code": "sj"
    },
    {
      "cnty_name": "Swaziland",
      "phone_code": "+268",
      "cnty_code": "sz"
    },
    {
      "cnty_name": "Sweden",
      "phone_code": "+46",
      "trunk_code": "0",
      "cnty_code": "se"
    },
    {
      "cnty_name": "Switzerland",
      "phone_code": "+41",
      "trunk_code": "0",
      "cnty_code": "ch"
    },
    {
      "cnty_name": "Syria",
      "phone_code": "+963",
      "trunk_code": "0",
      "cnty_code": "sy"
    },
    {
      "cnty_name": "Taiwan",
      "phone_code": "+886",
      "trunk_code": "0",
      "cnty_code": "tw"
    },
    {
      "cnty_name": "Tajikistan",
      "phone_code": "+992",
      "trunk_code": "8",
      "cnty_code": "tj"
    },
    {
      "cnty_name": "Tanzania",
      "phone_code": "+255",
      "trunk_code": "0",
      "cnty_code": "tz"
    },
    {
      "cnty_name": "Thailand",
      "phone_code": "+66",
      "trunk_code": "0",
      "cnty_code": "th"
    },
    {
      "cnty_name": "Togo",
      "phone_code": "+228",
      "cnty_code": "tg"
    },
    {
      "cnty_name": "Tokelau",
      "phone_code": "+690",
      "cnty_code": "tk"
    },
    {
      "cnty_name": "Tonga",
      "phone_code": "+676",
      "cnty_code": "to"
    },
    {
      "cnty_name": "Trinidad and Tobago",
      "phone_code": "+1868",
      "trunk_code": "1",
      "cnty_code": "tt"
    },
    {
      "cnty_name": "Tunisia",
      "phone_code": "+216",
      "cnty_code": "tn"
    },
    {
      "cnty_name": "Turkey",
      "phone_code": "+90",
      "trunk_code": "0",
      "cnty_code": "tr"
    },
    {
      "cnty_name": "Turkmenistan",
      "phone_code": "+993",
      "trunk_code": "8",
      "cnty_code": "tm"
    },
    {
      "cnty_name": "Tuvalu",
      "phone_code": "+688",
      "cnty_code": "tv"
    },
    {
      "cnty_name": "Uganda",
      "phone_code": "+256",
      "trunk_code": "0",
      "cnty_code": "ug"
    },
    {
      "cnty_name": "Ukraine",
      "phone_code": "+380",
      "trunk_code": "0",
      "cnty_code": "ua"
    },
    {
      "cnty_name": "United Arab Emirates",
      "phone_code": "+971",
      "trunk_code": "0",
      "cnty_code": "ae"
    },
    {
      "cnty_name": "United Kingdom",
      "phone_code": "+44",
      "trunk_code": "0",
      "cnty_code": "gb"
    },
    {
      "cnty_name": "United States",
      "phone_code": "+1",
      "cnty_code": "us"
    },
    {
      "cnty_name": "Uruguay",
      "phone_code": "+598",
      "trunk_code": "0",
      "cnty_code": "uy"
    },
    {
      "cnty_name": "Uzbekistan",
      "phone_code": "+998",
      "trunk_code": "0",
      "cnty_code": "uz"
    },
    {
      "cnty_name": "Vanuatu",
      "phone_code": "+678",
      "cnty_code": "vu"
    },
    {
      "cnty_name": "Venezuela",
      "phone_code": "+58",
      "trunk_code": "0",
      "cnty_code": "ve"
    },
    {
      "cnty_name": "Vietnam",
      "phone_code": "+84",
      "trunk_code": "0",
      "cnty_code": "vn"
    },
    {
      "cnty_name": "Wallis and Futuna",
      "phone_code": "+681",
      "cnty_code": "wf"
    },
    {
      "cnty_name": "Yemen",
      "phone_code": "+967",
      "trunk_code": "0",
      "cnty_code": "ye"
    },
    {
      "cnty_name": "Zambia",
      "phone_code": "+260",
      "trunk_code": "0",
      "cnty_code": "zm"
    },
    {
      "cnty_name": "Zimbabwe",
      "phone_code": "+263",
      "trunk_code": "0",
      "cnty_code": "zw"
    }
  ];
  
exports.cnty_data_source	= cnty_data_source;