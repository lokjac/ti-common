exports.open = function(){
	
	var scanditsdk = require("com.mirasense.scanditsdk");

	// disable the status bar for the camera view on the iphone and ipad
	if (Ti.Platform.osname == 'iphone' || Ti.Platform.osname == 'ipad') {
		Titanium.UI.iPhone.statusBarHidden = true;
	}

	var openScannerIfPermission = function() {
		if (Ti.Media.hasCameraPermissions()) {
			openScanner();
		} else {
			Ti.Media.requestCameraPermissions(function(e) {
				if (e.success) {
					openScanner();
				} else {
					alert('You denied camera permission.');
				}
			});
		}
	};

	// Sets up the scanner and starts it in a new window.
	var openScanner = function() {
		// First set the app key and which direction the camera should face.
		var SCANDIT_APP_KEY = "S7oFria3EeSbsRmCNNdBu7OkZDNB5ClSEjU+Xa1NiLw";
		var camera_facing = 0;
		scanditsdk.appKey = SCANDIT_APP_KEY;
		scanditsdk.cameraFacingPreference = camera_facing;

		// Only after setting the app key instantiate the Scandit SDK Barcode Picker view
		var picker = scanditsdk.createView({
			width : "100%",
			height : "100%"
		});
		// Before calling any other functions on the picker you have to call init()
		//picker.init();
		picker.init(SCANDIT_APP_KEY, camera_facing);

		// add a tool bar at the bottom of the scan view with a cancel button (iphone/ipad only)
		picker.showToolBar(true);

		// Create a window to add the picker to and display it.
		var window = Titanium.UI.createWindow({
			title : 'Scandit SDK',
			navBarHidden : true
		});

		// Set callback functions for when scanning succeeds and for when the
		// scanning is canceled. This callback is called on the scan engine's
		// thread to allow you to synchronously call stopScanning or
		// pauseScanning. Any UI specific calls from within this function
		// have to be issued through setTimeout to switch to the UI thread
		// first.
		picker.setSuccessCallback(function(e) {
			Titanium.API.debug('success scanned');
			picker.stopScanning();

			setTimeout(function() {
				window.close();
				window.remove(picker);
				alert("success (" + e.symbology + "): " + e.barcode);
				
				
				var url = "http://www.appcelerator.com";
				var client = Titanium.Network.createHTTPClient({
					// function called when the response data is available
					onload : function(e) {
						Ti.API.info("Received text: " + this.responseText);
						alert('success');
					},
					// function called when an error occurs, including a timeout
					onerror : function(e) {
						Ti.API.debug(e.error);
						alert('error');
					},
					timeout : 10000 // in milliseconds
				});
				// Prepare the connection.
				client.open("GET", url);
				// Send the request.
				client.send(); 
				
				
			}, 300);
		});
		picker.setCancelCallback(function(e) {
			picker.stopScanning();
			window.close();
			window.remove(picker);
		});

		window.add(picker);
		window.addEventListener('open', function(e) {
			picker.startScanning();
			// startScanning() has to be called after the window is opened.
		});
		window.open();
	};

	// Create button to open and start the scanner
	var start_scanner_btn = Titanium.UI.createButton({
		top: 100,
		width : 200,
		height : 80,
		title : "Start Scanner",
		font:{
			fontSize: 20
		},
	});
	
	var close_btn = Titanium.UI.createButton({
		top: 50,
		width : 200,
		height : 80,
		title : "Close Scanner",
		font:{
			fontSize: 20
		},
	});

	var rootWindow = Titanium.UI.createWindow({
		backgroundColor : '#FFFFFF',
		layout: 'vertical'
	});
	rootWindow.add(start_scanner_btn);
	rootWindow.add(close_btn);
	rootWindow.open();

	start_scanner_btn.addEventListener('click', function() {
		openScannerIfPermission();
		//openScanner();
	});
	
	close_btn.addEventListener('click', function() {
		rootWindow.close();
		rootWindow = null;
	});

	 

};
