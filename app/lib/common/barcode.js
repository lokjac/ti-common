var BARCODE_HTML_PATH = 'common/html/barcode.html';
var ANDROID_BARCODE_HTML_PATH = 'html/barcode.html';

var BarCodeView = function(code, $barcode_label, options) {
	
	var barcode_html_path = BARCODE_HTML_PATH;
	if(OS_ANDROID){
		barcode_html_path = ANDROID_BARCODE_HTML_PATH;
	}
	
	if (options === undefined) {
		options = {
			height: 30,
			width: 240,//Ti.UI.SIZE,
			url: barcode_html_path
		};
	}
	else {
		if (options.height === undefined)  options.height = 30;
		if (options.width === undefined) options.width = 240;//Ti.UI.SIZE;
		if (options.url === undefined) options.url = barcode_html_path;
	}
	
	var self = Ti.UI.createWebView(options);
	Ti.API.debug('options='+JSON.stringify(options));
	
	Ti.App.addEventListener('generateBarcode', function(){
		self.evalJS("doBarcode(unescape('" + code + "'), "+options.height+")");
		var barcode_code = self.evalJS("getBarcode()");
		Ti.API.debug('barcode_code='+barcode_code);
		$barcode_label.text = barcode_code;
		
	});
	
	return self;
};

module.exports = BarCodeView;