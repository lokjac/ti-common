function executeIOSLocalNotification(message_from_caller) {
	var stop_background_service = Ti.App.Properties.getString('stop_background_service');
	if (stop_background_service) {
		//Ti.App.currentService.unregister();
		Ti.API.debug('after calling service unregister');
		Ti.App.currentService.stop();
	} else {
		//Ti.App.iOS.setMinimumBackgroundFetchInterval(Ti.App.iOS.BACKGROUNDFETCHINTERVAL_MIN);
		
		Ti.API.debug('Ti.App.iOS.BACKGROUNDFETCHINTERVAL_MIN=' + Ti.App.iOS.BACKGROUNDFETCHINTERVAL_MIN);
		
		var count 		= Ti.App.Properties.getInt('bg-svc-count', 0);
		count++;
		var title 		= 'Hello World Notification '+count;
		var message 	= 'bg-service: has been invoked ' + count + ' times. It is still registered and will run again when the app is transitioned to the background';
		if(message_from_caller){
			message = message_from_caller;
		}
		var curNotif = Ti.App.iOS.scheduleLocalNotification({
			category : title,
			alertBody : message,
			repeat : "daily",
			userInfo : {
				controller : "show_something",
				message : message,
				id : count

			},
			date : new Date(new Date().getTime() + 1000) // 1 second after pause
		});
		Ti.API.debug('after created notification for iOS');
		//Ti.API.info('message='+message);
	}

	var listener = Ti.App.currentService.addEventListener('stop', function() {
		Ti.API.debug('iOS background service has been stopped');
	});
}


function convertLetterToNumber(str) {
	if (str) {
		str = str.replace(/[^A-Za-z 0-9]*/g, '');
		var out = 0,
		    len = str.length;
		for ( pos = 0; pos < len; pos++) {
			out += (str.charCodeAt(pos) - 64) * Math.pow(26, len - pos - 1);
		}
		return out;
	} else {
		return null;
	}
}

function createQRCodeImage(text, size, load_callback){
	var default_size=150;
	var width = default_size, height=default_size;
	if(size){
		width 	= size;
		height 	= size;
	}
	if (Ti.Android) {
	    width += 'dp';
	    height += 'dp';
	}
	var url = getQRCodeGeneratorURL(text, size);
	
	Ti.API.debug('qr_code url='+url);
	var qr_code_image = Ti.UI.createImageView({
	  image			: url,
	  width			: width,
	  height		: height,
	  //load_callback	: load_callback
	}); 
	
	cachedImageView(getQRCodeCacheDirectoryPath(), qr_code_image.image, qr_code_image);
    function loadImage(e){
    	Ti.API.debug('QR code image is loaded');
    	if(load_callback){
	    	load_callback();
	    }
    }
    qr_code_image.addEventListener('load',loadImage);
    
    return qr_code_image;
};

function formatIntlDatetimeString(datetime_in_str, date_format){
	if(datetime_in_str!=null && datetime_in_str.length>=16){
		if(date_format==null){
			date_format = 'medium';
		}
		var minute_str 	= datetime_in_str.substring(14,16);
		var minute_int 	= parseInt(minute_str, 10)-1;
		var hour_str 	= datetime_in_str.substring(11,13);
		var hour_int 	= parseInt(hour_str, 10)-1;
		var date_str 	= datetime_in_str.substring(0,2);
		var month_str 	= datetime_in_str.substring(3,5);
		var month_int 	= parseInt(month_str, 10)-1;
		var year_str 	= datetime_in_str.substring(6,10);
		
		var _date = new Date(Date.UTC(year_str, month_int, date_str, hour_int, minute_int));
		
		
		var formatted_datetime = String.formatTime(_date, date_format);
		
		return formatted_datetime;
	}
	return datetime_in_str;
}

function formatString(text){
	//check if there are two arguments in the arguments list
    if (arguments.length <= 1) {
        //if there are not 2 or more arguments there's nothing to replace
        //just return the text
        return text;
    }
    //decrement to move to the second argument in the array
    var tokenCount = arguments.length - 2;
    for (var token = 0; token <= tokenCount; ++token) {
        //iterate through the tokens and replace their placeholders from the text in order
        text = text.replace(new RegExp("\\{" + token + "\\}", "gi"), arguments[token + 1]);
    }
    return text;
}

exports.formatString				= formatString;
exports.formatIntlDatetimeString	= formatIntlDatetimeString;
exports.executeIOSLocalNotification = executeIOSLocalNotification;
exports.convertLetterToNumber		= convertLetterToNumber;