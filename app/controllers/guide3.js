var args 			= arguments[0] || {};
var caller_window	= args.caller_window;


init();

function init(){
	$.close_btn.addEventListener('click', function(){
		if(caller_window){
			caller_window.close();
			caller_window = null;
		}
	});
}
