var data 		= require('common/data');
var common_util = require('common/common_util');

init();

function init(){
	
	initCountryCustomPicker();
	initMobileInput();
	initButton();
	$.index.open({fullscreen:true});	
	$.show_datetime.text = common_util.formatIntlDatetimeString('14/01/2016 06:40');
	$.date_input.init({
		window: $.index,
		value:'01/05/2016'
	});
	
}

function initCountryCustomPicker(){
	var dummy_data_source = [];
	for(var i=0; i<5; i++){
		dummy_data_source.push({
			code: i+1, label:i+1
		});
	}
	
	var after_close_or_selected = function(selected_option){
	};
	/*
	$.show_custom_picker.init({
		title: 'Select an option',
		data_source: dummy_data_source,
		option_code_field_name:'code',
		option_label_field_name:'label', 
		callback_after_selected: after_close_or_selected,
		callback_after_cancel: after_close_or_selected,
		value: '2',
		
	});
	
	$.show_custom_picker1.init({
		title: 'Select an country',
		data_source: data.cnty_data_source,
		option_code_field_name:'cnty_code',
		option_label_field_name:'cnty_name', 
		callback_after_selected: function(selected_option){
			if(selected_option){
					
			}
			
		},
		callback_after_cancel: after_close_or_selected,
		value: 'my',
		show_filter: true,
	});
	
	$.show_custom_picker2.init({
		title: 'Select an country',
		data_source: data.cnty_data_source,
		option_code_field_name:'cnty_code',
		option_label_field_name:'cnty_name', 
		callback_after_selected: function(selected_option){
			if(selected_option){
					
			}
		},
		callback_after_cancel: after_close_or_selected,
		value: 'sg',
		show_filter: true,
	});
	*/
	$.show_custom_pickerx.init({
		title: 'Select an option',
		data_source: dummy_data_source,
		option_code_field_name:'code',
		option_label_field_name:'label', 
		callback_after_selected: function(selected_option){
			if(selected_option){
					
			}
			
		},
		callback_after_cancel: after_close_or_selected,
		value: '3',
		show_filter: false,
		
	});
}

function initMobileInput(){
	$.mobile_input.init({
		cnty_data_source			: data.cnty_data_source,
		cnty_code_field_name		: 'cnty_code',
		cnty_label_field_name		: 'cnty_name',
		cnty_trunk_code_field_name	: 'trunk_code',
		cnty_phone_code_field_name	: 'phone_code',
		default_cnty_code			: 'my'
	});
}

function initButton(){
	$.view_about_btn.addEventListener('click', function(){
		var about_window = Alloy.createController('about',{
		}).getView();
		about_window.open({modal:true});
	});
	
	$.view_guide_btn.addEventListener('click', function(){
		var guide_window = Alloy.createController('guide').getView();
		guide_window.open({modal:true});
	});
	
	$.show_qr_code_btn.addEventListener('click', function(){
		var qrcode_window = Alloy.createController('qrcode_view').getView();
		qrcode_window.open({modal:true});
	});
	
	$.show_bar_code_btn.addEventListener('click', function(){
		var barcode_window = Alloy.createController('barcode_view').getView();
		barcode_window.open({modal:true});
	});
	$.reset_btn.addEventListener('click', function(){
		var new_index = Alloy.createController('index').getView();
		new_index.open();
		
		$.index.close();
		$.index = null;
	});
	$.demo_socket_btn.addEventListener('click', function(){
		var socket_demo = Alloy.createController('socket_demo').getView();
		socket_demo.open();
		
		$.index.close();
		$.index = null;
	});
	
	$.show_scanner_btn.addEventListener('click', function(){
		var scanner = require('common/scanner');
		scanner.open();
	});
	
	$.show_nfc_reader_btn.addEventListener('click', function(){
		var nfc_reader_window = Alloy.createController('nfc_reader').getView();
		nfc_reader_window.open({modal:true});
	});
	
	
}
