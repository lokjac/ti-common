// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var nfc 		= require("ti.nfc");
var args 		= arguments[0] || {};
var nfcAdapter	= null;
init();

function init(){
	
	initButton();
}

function initButton(){
	$.close_btn.addEventListener('click', function(){
		$.nfc_reader.close();
		var act = $.nfc_reader.activity;
		act.removeListener('newintent', newIntent);
		act.removeListener('resume', resumeNFC);
		act.removeListener('pause', pauseNFC);
		nfcAdapter.disableForegroundDispatch();
		
		nfcAdapter = null;
		$.nfc_reader.close();
		$.nfc_reader = null;
	});
	$.clear_btn.addEventListener('click', function(){
		Ti.API.debug('Going to clear NFC tag id');
		$.nfc_tag_id.text = '';
	});
}

function newIntent(e){
	if(nfcAdapter){
		nfcAdapter.onNewIntent(e.intent);
	}
}

function resumeNFC(e){
	Ti.API.debug('after resumed nfc reader');
	if(nfcAdapter){
		nfcAdapter.enableForegroundDispatch(dispatchFilter);
	}
}

function pauseNFC(e){
	Ti.API.debug('after paused nfc reader');
	if(nfcAdapter){
    	nfcAdapter.disableForegroundDispatch();
   	}
}

function setupNfc() {
	// Create the NFC adapter to be associated with this activity. 
	// There should only be ONE adapter created per activity.
	nfcAdapter = nfc.createNfcAdapter({
		 onNdefDiscovered: handleNDEFDiscovery,
	     onTagDiscovered: handleTagDiscovery,
	     onTechDiscovered: handleTechDiscovery
	});
	
	// It's possible that the device does not support NFC. Check it here
	// before we go any further.
	if (!nfcAdapter.isEnabled()) {
		alert('NFC is not enabled on this device');
		return;
	}else{
		//var act = Ti.Android.currentActivity;
		var act = $.nfc_reader.activity;
        act.addEventListener('newintent', newIntent);

        // Since we want the app to only use NFC while active in the foreground
        // We disable and enable foreground dispatch on app pause and resume respectively
        
        act.addEventListener('resume', resumeNFC);
        
        act.addEventListener('pause', pauseNFC);
        
        // The foreground dispatch filter specifies the types of NFC 
        // messages the app wants to receive and handle. The only entry in our case
        // specifies type ‘text/plain’ since we want to send and receive plain text
        /*
        dispatchFilter = nfc.createNfcForegroundDispatchFilter({
            intentFilters: [
                { action: nfc.ACTION_NDEF_DISCOVERED, mimeType: 'text/plain' },
            ],
            techLists: [
                [ "android.nfc.tech.NfcF" ],
                [ "android.nfc.tech.Ndef" ],
                [ "android.nfc.tech.MifareClassic" ],
                [ "android.nfc.tech.NfcA" ]
            ]
        });
        */
        dispatchFilter = nfc.createNfcForegroundDispatchFilter({
		    intentFilters: [
		        { action: nfc.ACTION_TECH_DISCOVERED },
		    ],
		    techLists: [
		    	["android.nfc.tech.IsoDep"],
		    	["android.nfc.tech.NfcA"],
		    	["android.nfc.tech.NfcB"],
		    	["android.nfc.tech.NfcF"],
		    	["android.nfc.tech.NfcV"],
		        [ "android.nfc.tech.Ndef" ],
		        [ "android.nfc.tech.NdefFormatable" ],
                [ "android.nfc.tech.MifareClassic" ],
                [ "android.nfc.tech.MifareUltralight" ]
		    ]
		});
        
        // This enables foreground dispatch for the first time
        nfcAdapter.enableForegroundDispatch(dispatchFilter);
	}
	
	// All tag scans are received by the activity as a new intent. Each
	// scan intent needs to be passed to the nfc adapter for processing.
	
	//var act = Ti.Android.currentActivity;
	//var act = $.nfc_reader.activity;
	//act.addEventListener('newintent', function(e) {
	//	nfcAdapter.onNewIntent(e.intent);
	//});
}

function handleNDEFDiscovery(data){
	if(data!=null && data.messages[0]!=null && data.messages[0].records[0]!=null){
		alert('NDEF DISCOVERED:: ' + data.messages[0].records[0].getPayload());	
    
	    // Our required payload fits in the first record of the first Ndef msg
	    var payload = data.messages[0].records[0].getPayload();
	    // First byte of payload is control byte
	    // Bits 5 to 0 of the control bytes contain 
	    // length of language code succeeding the control byte
	    var langCodeLength = payload[0] & 0077;
	    // Received NFC text data starts after textOffset bytes in payload
	    var textOffset = langCodeLength + 1;
	    // Payload contains byte array which needs to be converted to a string
	    // nfc_text contains the exact text string sent by the sending NFC device
	    var nfc_text = payload.toString().substring(textOffset);
	    /* process nfc_text as required by application logic here */
  	}
}

function handleTagDiscovery(data){
	if(data!=null && data.messages[0]!=null && data.messages[0].records[0]!=null){
    	alert('TAG DISCOVERED:: ' + data.messages[0].records[0].getPayload());
    }
}

function handleTechDiscovery(data){
    //alert('TECH DISCOVERED:: ' + data.messages[0].records[0].getPayload());
    var tag = data.tag;
    Ti.API.debug('data='+JSON.stringify(data));
    Ti.API.debug('tag='+JSON.stringify(tag));
    //alert('Tag ID: ' + tag.getId());
    updateTagId(tag.getId());
}

function updateTagId(tag_id){
	$.nfc_tag_id.text = tag_id;
}
