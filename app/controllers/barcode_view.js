var barcode_webview		= require("common/barcode");
var args 				= arguments[0] || {};

init();

function init(){
	var reference_no = "1122334455";
	/*
	var sample_barcode_view = new barcode_webview(reference_no, $.barcode_code, {
			height: 100
	});
	$.bar_code_view.add(sample_barcode_view);
	*/
	$.barcode_view.init(reference_no, {
		height: 40
	});
	initButton();
}

function initButton(){
	$.close_btn.addEventListener('click', function(){
		$.barcode.close();
		$.barcode = null;
	});
}
