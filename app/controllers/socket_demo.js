var server_util = require('common/server_util');
var args = arguments[0] || {};

init();

function init(){
	initButtons();
}

function initButtons(){
	/*
	$.create_socket_btn.addEventListener('click', function(){
		var port_no 	= $.server_port_no.value || 12345;
		var socket_ip 	= $.server_ip.value || '127.0.0.1';
		server_util.createSocket(socket_ip, port_no, 10, createSocketResponse);
	});
	*/
	$.connect_socket_btn.addEventListener('click', function(){
		var port_no 	= $.server_port_no.value || 12345;
		var socket_ip 	= $.server_ip.value || '127.0.0.1';
		server_util.connectSocket(socket_ip, port_no, connectSocketCallback);
		
	});
	
	$.check_socket_status_btn.addEventListener('click', function(){
		var status = server_util.checkSocketStatus();
		alert(status);
	});
	
	$.close_socket_btn.addEventListener('click', function(){
		server_util.closeSocket();
		$.send_socket_btn.visible = false;
	});
	
	$.send_socket_btn.addEventListener('click', function(){
		var socket_input 	= $.test_send_input.value || 'ping';
		server_util.writeRequest(socket_input);
	});
	
	$.back_btn.addEventListener('click', function(){
		var index = Alloy.createController('index').getView();
		index.open();
		
		$.socket_demo.close();
		$.socket_demo = null;
	});
	Ti.API.debug('Ti.Platform.address='+Ti.Platform.address);
	$.server_ip.value = Ti.Platform.address;
}

function createSocketResponse(request){
	alert(request);
	var status_response = {status: 1};
	return 	JSON.stringify(status_response);
}

function hex2a(hexx) {
    var hex = hexx.toString();//force conversion
    var str = '';
    for (var i = 0; i < hex.length; i += 2)
        str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
    return str;
}

function hex2Ascii(valueStr, seperator){
	var symbols = " !\"#$%&'()*+,-./0123456789:;<=>?@";
    var loAZ = "abcdefghijklmnopqrstuvwxyz";
    symbols += loAZ.toUpperCase();
    symbols += "[\\]^_`";
    symbols += loAZ;
    symbols += "{|}~";
    
    var hexChars = "0123456789abcdef";


	valueStr = valueStr.toLowerCase();
    var text = "";
    var i=0;

    for( i=0; i<valueStr.length; i=i+2 )
    {
        var char1 = valueStr.charAt(i);
        if ( seperator && char1 == seperator )
        {
            i++;
            char1 = valueStr.charAt(i);
        }
        var char2 = valueStr.charAt(i+1);
        var num1 = hexChars.indexOf(char1);
        var num2 = hexChars.indexOf(char2);
        var value = num1 << 4;
        value = value | num2;

        var valueInt = parseInt(value);
        var symbolIndex = valueInt - 32;
        var ch = '?';
        if ( symbolIndex >= 0 && value <= 126 )
        {
            ch = symbols.charAt(symbolIndex);
        }
        text += ch;
    }

    return text;
}

function stupidTest(){
	var result = '';
	var _a = [0xde,0xc0,0xad,0xde,0xde,0xc0,0xad,0xde,0x05,0x00,0x02,0x00,0x50,0x01,0x00,0x00,0xe8,0x03,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00];
	for(var i=0; i<_a.length; i++){
		result+=String.fromCharCode(_a[i]);
	}
	Ti.API.debug('result='+result);
	return result;
}

function connectSocketCallback(){
	var test_src = '7061756C';
	var encoded_input_src = 'dec0addedec0adde0500020050010000e80300000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000';
	var decoded_input_src = '\xde\xc0\xad\xde\xde\xc0\xad\xde\x05\x00\x02\x00P\x01\x00\x00\xe8\x03\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00';
	
	//var decoded_input_src = hex2Ascii(encoded_input_src);
	//var first_input = Ti.Codec.decodeString({ source: input_src });
	//var first_input = hex2a(input_src);
	/*
	var buffer = Ti.createBuffer({ length: 1024 });
	var length = Ti.Codec.encodeString({
	    source: decoded_input_src,
	    dest: buffer,
	    //charset: Ti.Codec.TYPE_BYTE
	});
	buffer.length = length;
	*/
	//var string = Ti.Codec.decodeString({ source: decoded_input_src, charset: Ti.Codec.TYPE_BYTE });
	//var bytestring = hexStringToByte(encoded_input_src);
	//var buffer = Ti.createBuffer({ value:string, type: Ti.Codec.TYPE_BYTE });
	
	//Ti.API.debug('string='+string);
	//server_util.writeRequestBuffer(buffer, function(){
	server_util.writeRequest(stupidTest(), function(){	
		$.send_socket_btn.visible = true;	
	});
}
