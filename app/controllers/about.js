var args = arguments[0] || {};

init();

function init(){
	initButton();
	$.about.open();
}

function initButton(){
	$.close_btn.addEventListener('click', function(){
		$.about.close();
		$.about = null;
	});
}
