var qrcode_webview		= require("common/qrcode");
var args 				= arguments[0] || {};

init();

function init(){
	var reference_no = "1234567890";
	$.qr_code_view.init({
		text: reference_no,
		height: 200,
		width: 200,
		top: 20,
	});
	initButton();
}


function initButton(){
	$.close_btn.addEventListener('click', function(){
		$.qrcode.close();
		$.qrcode = null;
	});
}