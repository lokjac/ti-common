// The contents of this file will be executed before any of
// your view controllers are ever executed, including the index.
// You have access to all functionality on the `Alloy` namespace.
//
// This is a great place to do any initialization for your app
// or create any global variables/functions that you'd like to
// make available throughout your app. You can easily make things
// accessible globally by attaching them to the `Alloy.Globals`
// object. For example:
//
// Alloy.Globals.someGlobalFunction = function(){};

Ti.App.addEventListener('resumed',function() {
	Ti.API.debug('============ resumed event ==============');
	if (OS_ANDROID) {
		Alloy.Globals.url = Ti.Android.currentActivity.intent.data;
	}else
	if(OS_IOS){
    	Alloy.Globals.url = Ti.App.getArguments().url;
    }
    Ti.API.debug('>>>>>>> Globals.url='+Alloy.Globals.url);
    
    var nfc_reader_window = Alloy.createController('nfc_reader').getView();
	nfc_reader_window.open({modal:true});
});