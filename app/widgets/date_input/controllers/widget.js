var DATE_SEPARATOR = "/";

function init(args){
	Ti.API.debug('----date_input: initConfig----, args='+JSON.stringify(args));
	var exclude = ['window'];
	var $window = args.window;
	$.date_textfield.applyProperties( _.omit(args, exclude) );
	
	initEvent($window);
	
}

function initEvent($window){
	
	$.date_input.addEventListener('click', function(e) {
		Ti.API.info('datepicker=' + $window.datepicker);
		if ($window.datepicker == null) {
			$window.datepicker = createDatePicker($.date_textfield);
			if (OS_ANDROID) {
				showDatePickerDialog($window.datepicker, $.date_textfield);
			} else if (OS_IOS) {
				$window.datepicker.setVisible(true);
				$window.add($window.datepicker);
			}
		} else {
			if (OS_ANDROID) {
				showDatePickerDialog($window.datepicker, $.date_textfield);
			} else if (OS_IOS) {
				if ($window.datepicker.visible) {
					var date_value = $window.datepicker.getValue();
					birthdate_input.setValue(parseDate2String(date_value));
					$window.datepicker.hide();
				} else {
					$window.datepicker.showAgain();
				}
			}

		}
	}); 

}

/**
 * Property for manage current value
 */
Object.defineProperty($, "value", {
	set : setValue,
	get : getValue
});

/**
 * Get selected item value
 * @return {String} Selected item value
 */
function getValue() {
	return $.date_textfield.value;
};

/**
 * Set selected item value
 * @param {String} value
 */
function setValue(value) {
	$.date_textfield.value = value;
}


function reset() {
	!_.isUndefined(defaultValue) && setValue(defaultValue);
};

function createDatePicker(date_input){
	Ti.API.info('calling createDatePicker');
	date_input.blur();
	var date_value = parseDatepickerInput(date_input);
	Ti.API.info('date_value after parseDatepickerInput='+date_value);	
	
	var datepicker = Ti.UI.createPicker({
	        type:Ti.UI.PICKER_TYPE_DATE,
	        //useSpinner for Android Only
	        useSpinner: true,
	        value:date_value,
	        //value: new Date(),
	        top: "25%",
	        width: Ti.UI.FILL,
	        height: Ti.UI.SIZE,
	        borderRadius: 6,
	        borderColor: "#222",
	        borderWidth: 1,
	        
	});
	
	Ti.API.info('date_value from datepicker='+datepicker.getValue());
	
	if(OS_IOS){
		var picker_view = Titanium.UI.createView({
			height: Ti.UI.FILL,
			width: Ti.UI.FILL,
			//width: '90%',
			backgroundColor: "#000000",
			bottom:0,
			zIndex: 9999999,
			
		});
		
		var cancel =  Titanium.UI.createButton({
			title:'Cancel',
			style:Titanium.UI.iPhone.SystemButtonStyle.BORDERED
		});
		 
		var done =  Titanium.UI.createButton({
			title	: L('done'),
			style	: Titanium.UI.iPhone.SystemButtonStyle.DONE
		});
		
		var spacer =  Titanium.UI.createButton({
			systemButton:Titanium.UI.iPhone.SystemButton.FLEXIBLE_SPACE
		});
		
		var toolbar =  Ti.UI.iOS.createToolbar({
			top:0,
			items:[cancel,spacer,done]
		});
		
		done.addEventListener('click',function() {
			var date_value = datepicker.getValue();
			Ti.API.info('date_value after clicked done button='+date_value);
			var parsed_string = parseDate2String(date_value);
			Ti.API.info('parsed_string='+parsed_string);
			date_input.setValue(parsed_string);
			picker_view.hide();
		});
		
		cancel.addEventListener('click',function() {
			picker_view.hide();
		});
		
		picker_view.add(toolbar);
		picker_view.add(datepicker);
		
		picker_view.showAgain = function(){
			date_input.blur();
			showDatePickerAgain(datepicker, date_input);
			picker_view.show();
		};
		
		return picker_view;
	}else{
		return datepicker;
	}
	
	
}

function showDatePickerAgain(datepicker, date_input){
	try{
		datepicker.value = parseDatepickerInput(date_input);	
	}catch(error){
		Ti.API.error('Failed to show date picker again');
	}
}

/*
 * Only support fixed date format in dd/mm/yyyy
 */
function parseDatepickerInput(date_input) {
	if(date_input){
		var date_value_str = date_input.getValue();
		Ti.API.info('date_value_str='+date_value_str);
		var parsed_date = null;
		if (date_value_str == null || date_value_str.trim() == '') {
			parsed_date = new Date();
			Ti.API.info('parsed_date new Date()='+parsed_date);
		} else {
			parsed_date = parseString2Date(date_value_str);
			Ti.API.info('parsed_date after parseString2Date='+parsed_date);
		}
		return parsed_date;
	}else{
		return new Date();
	}
}

function parseString2Date(date_value, date_separator){
	if(date_separator==null){
		date_separator = DATE_SEPARATOR;
	}
	var parts 			= date_value.split(date_separator);
	var year_value 		= parseInt(parts[2], 10);
	var month_value 	= parseInt(parts[1], 10) - 1;
	var day_date_value 	= parseInt(parts[0], 10);
	
	Ti.API.info('year_value='+year_value);
	Ti.API.info('month_value='+month_value);
	Ti.API.info('day_date_value='+day_date_value);
	
	try{
		Ti.API.info(day_date_value + date_separator + month_value + date_separator + year_value);
		if(isNaN(day_date_value) || isNaN(month_value) || isNaN(year_value)){
			return new Date();
		}else{
			//var parsed_date = new Date(year_value, month_value, day_date_value);
			var parsed_date = null;
			//if(OS_IOS){
			parsed_date = new Date(Date.UTC(year_value, month_value, day_date_value));	
			//}else{
			//	parsed_date = new Date(year_value, month_value, day_date_value);
			//}
			
			return parsed_date;
		}
		
	}catch(error){
		Ti.API.error('Failed to parse date from string, where date_value='+date_value);
		return new Date();
	}
}

function parseDate2String(date_value){
	if(date_value){
		Ti.API.info('date_value='+date_value);
		
		var day_date_str = date_value.getDate();
		Ti.API.info('day_date_str='+day_date_str);
		if(day_date_str<10){
			//day_date_str = "0"+(parseInt(day_date_str) - 1 );
			day_date_str = "0"+(parseInt(day_date_str) );
		}
		var month_str = date_value.getMonth() +1;
		if(month_str<10){
			month_str = "0"+month_str;
		}
		
		return day_date_str + DATE_SEPARATOR + month_str + DATE_SEPARATOR + date_value.getFullYear();
	}else{
		return "";
	}
	
}


function showDatePickerDialog(datepicker, input_element, callback) {
	if (datepicker) {
		input_element.focus();
		datepicker.showDatePickerDialog({
			value : parseDatepickerInput(input_element),
			callback : function(e) {
				input_element.blur();
				if (e.cancel) {
					Ti.API.info('User canceled dialog');
				} else {
					Ti.API.info('User selected date: ' + e.value);
					var date_value = e.value;
					var day_date = date_value.getDate();
					if(day_date<10){
						day_date = "0"+day_date;
					}
					var month_value =date_value.getMonth() + 1;
					if(month_value<10){
						month_value ="0" + month_value;
					}
				  	var birthdate_str = day_date + DATE_SEPARATOR + month_value + DATE_SEPARATOR + date_value.getFullYear();
				  	
				  	input_element.setValue(birthdate_str);
					if (callback) {
						callback();
					}

				}
			}
		});
	}

}

exports.init 		= init;
exports.setValue 	= setValue;
exports.getValue 	= getValue;
exports.reset 		= reset;