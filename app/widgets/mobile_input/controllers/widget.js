var args							= arguments[0] || {};
var WINDOW_CONTENT_TOP_PHONE		= 41;
var WINDOW_CONTENT_TOP_TABLET		= 50;
var LOADING_PICKER_BG_COLOR			= "#A9A9A9";
var LOADING_PICKER_FONT_COLOR		= "#FFFFFF";
var BACKGROUND_COLOR__WHITE			= "#FFFFFF";
var picker_data_source				= null;
var label_ori_color					= "#555555";
var label_ori_text					= null;
var is_mobile_input_modal_triggered	= false;
var config_options					= args;
var cnty_data_source				= null;
var default_cnty_code				= null;
var phone_number					= '';

/*
 * 	options param:
 * 		cnty_data_source - Country data source 
 * 		callback_after_completed - Call back function after completed phone number entered
 * 		callback_after_remove	- Call back function after remove entered phone number
 * 		default_cnty_code - Default country code
 * 
 * 
 */
function init(options){
	initConfig(options);
	initButton();
}

function initConfig(options){
	Ti.API.debug('----mobile_input: initConfig----');
	if(options){
		Object.keys(options).forEach(function(key) {
			config_options[ key ] = options[ key ];
		}); 
	}
	if(config_options.text){
		$.mobile_no_label.text 		= config_options.text;
		label_ori_text				= config_options.text;
	}
	if(config_options.color){
		$.mobile_no_label.color 	= config_options.color;
		label_ori_color 			= config_options.color;
	}
	if(config_options.width){
		$.mobile_input_view.width 	= config_options.width;
	}
	if(config_options.cnty_data_source){
		cnty_data_source = config_options.cnty_data_source;
	}
	if(config_options.default_cnty_code){
		default_cnty_code = config_options.default_cnty_code;
	}
	if(config_options.value){
		phone_number 				= config_options.value;
		$.mobile_no_label.text 		= config_options.value;
		$.write_mobile_no_button.visible	= false;	
		$.remove_mobile_no_button.visible 	= true;
	}
}

function initButton(){
	$.mobile_input_view.addEventListener('click', function(){
		if($.remove_mobile_no_button.visible==false){
			Ti.API.debug('entered write mobile number button');
			$.mobile_input_view.backgroundColor 	= LOADING_PICKER_BG_COLOR;
			$.mobile_no_label.color 				= LOADING_PICKER_FONT_COLOR;
			$.mobile_no_label.text					= L('loading');
			if(is_mobile_input_modal_triggered==false){
				initMobileInputModalWindow();
				is_mobile_input_modal_triggered = true;
			}
		}else{
			$.mobile_no_label.text			  	= label_ori_text;
			phone_number					  	= null;
			$.write_mobile_no_button.visible 	= true;
			$.remove_mobile_no_button.visible 	= false;
			if(config_options.callback_after_remove){
				config_options.callback_after_remove();
			}
		}
	});
}

function initMobileInputModalWindow(){
	var callback_after_closed = function(){
		/*
		if(config_options.callback_after_completed){
			config_options.callback_after_completed();
		}
		*/
	};
	
	var callback_after_opened = function(){
		Ti.API.debug('callback after opened');
		is_mobile_input_modal_triggered = false;
		$.mobile_input_view.backgroundColor 	= BACKGROUND_COLOR__WHITE;
		$.mobile_no_label.color 				= label_ori_color;
		$.mobile_no_label.text					= label_ori_text;
		
		
	};
	
	var callback_after_removed_phone_number = function(entered_phone_number){
		Ti.API.debug('initMobileInputModalWindow: callback after removed phone number, label_ori_text='+label_ori_text);
		$.mobile_no_label.text 	= label_ori_text;
		phone_number 			= null;
		
	};
	
	var callback_after_entered_phone_number = function(entered_phone_number){
		Ti.API.debug('initMobileInputModalWindow: callback after entered phone number, entered_phone_number='+entered_phone_number);
		if(entered_phone_number){
			$.mobile_no_label.text 	= entered_phone_number;
			phone_number 			= entered_phone_number;
			$.write_mobile_no_button.visible	= false;	
			$.remove_mobile_no_button.visible 	= true;
			
			Ti.API.debug('callback_after_entered_phone_number: enter phone_number='+phone_number);
			if(config_options.callback_after_completed){
				config_options.callback_after_completed(phone_number);	
			}
		}
	};
	
	createPopupModalViewWindow(
							'mobile_input_view', 
							L('enter.mobile.no'), 
							{
								cnty_data_source			: config_options.cnty_data_source,
								cnty_code_field_name		: config_options.cnty_code_field_name,
								cnty_label_field_name		: config_options.cnty_label_field_name,
								cnty_trunk_code_field_name	: config_options.cnty_trunk_code_field_name,
								cnty_phone_code_field_name	: config_options.cnty_phone_code_field_name,
								callback_after_completed	: callback_after_entered_phone_number,
								callback_after_removed		: callback_after_removed_phone_number,
								default_cnty_code			: default_cnty_code
							},
							callback_after_closed,
							callback_after_opened
	);
}

function createNavigationBar(back_button_callback, title){
	var navigation_bar = Widget.createController("navigation_bar",{
		title: title,
		back_button_callback: back_button_callback
	});
	return navigation_bar;
}

function createPopupModalViewWindow(view_path, title, params, callback_after_closed, callback_after_opened){
	var modalWindow = Ti.UI.createWindow({
							modal				: OS_ANDROID? false: true, 
							backgroundColor		: BACKGROUND_COLOR__WHITE, 
							layout				: "absolute",
							height				: Ti.UI.FILL,
							width				: Ti.UI.FILL,
							fullscreen			: true,
							navBarHidden		: true
						});
	
	if(OS_ANDROID){
		modalWindow.windowSoftInputMode = Titanium.UI.Android.SOFT_INPUT_STATE_HIDDEN;	
	}
	
	
	var navigation_view = Ti.UI.createView({
		width	: Ti.UI.FILL,
	    layout	: 'vertical',
	    height	: Ti.UI.SIZE,
	    top		: 0,
	});
	
	var close_modal_function = null;
	if(params && params['close_modal_function']){
		close_modal_function = params['close_modal_function'];
	}
	
	var close_modal = function(){
		if(modalWindow){
			modalWindow.close();
			modalWindow = null;
			Ti.API.debug('Modal window is closed properly');
		}
	};
	modalWindow.addEventListener('open', function(){
		Ti.API.debug('Modal window is opened');
		if(callback_after_opened){
			callback_after_opened();
		}				
	});
	
	modalWindow.addEventListener('close', function(){
		Ti.API.debug('close modal window');
		if(callback_after_closed && (modalWindow==null || modalWindow.ignore_close_callback==false)){
			callback_after_closed();
			Ti.API.debug('call callback after closed');
		}else{
			Ti.API.debug('ignore to call callback after closed');
		}				
	});
	
	var navigation_bar = createNavigationBar(function(){
			if(close_modal_function){
				close_modal_function();
			}else{
				close_modal();
			}
		},
		title
	);
	
	//navigation_view.add(navigation_view);
	modalWindow.add(navigation_bar.getView());
	
	var navigation_title = navigation_bar.navigation_title;
	
	var view_params = {};
	if(params){
		view_params = params;	
	}
	view_params['modal_window'] = modalWindow;
	
	Ti.API.debug('params='+JSON.stringify(params));
	
	view_params['navigation_bar'] 	= navigation_bar;
	view_params['navigation_title'] = navigation_title;
	
	var content_view = Widget.createController(view_path, view_params).getView();
	Ti.API.debug('content_view='+content_view);
	if(Alloy.isTablet){
		content_view.setTop(WINDOW_CONTENT_TOP_TABLET);	
	}else{
		content_view.setTop(WINDOW_CONTENT_TOP_PHONE);
	}
			
	content_view.window 	= modalWindow;
	modalWindow.close_modal	= close_modal;
	
	modalWindow.add(navigation_view);
	modalWindow.add(content_view);
	modalWindow.open();
	
	return modalWindow;
}

function setValue(value){
	if(value!=null){
		phone_number = value;	
	}
}

function getValue(){
	return phone_number;
}

exports.init 		= init;
exports.value 		= phone_number;
exports.setValue	= setValue;
exports.getValue	= getValue;