var args 							= arguments[0] || {};
var modal_window					= args.modal_window;
var cnty_data_source				= args.cnty_data_source;
var default_cnty_code				= args.default_cnty_code;
var cnty_code_field_name			= args.cnty_code_field_name;
var cnty_label_field_name			= args.cnty_label_field_name;
var cnty_trunk_code_field_name		= args.cnty_trunk_code_field_name;
var cnty_phone_code_field_name		= args.cnty_phone_code_field_name;
var callback_after_removed			= args.callback_after_removed;
var callback_after_completed		= args.callback_after_completed;
var selected_cnty_trunk_code		= null;
var selected_phone_code				= null;

initMobileInputView();

function initMobileInputView(){
	initButton();
	initCountryPicker();
}

function initCountryPicker(){
	$.country_picker.init({
		title					: L('select.country'),
		data_source				: cnty_data_source,
		value					: default_cnty_code,
		caller_window			: modal_window,
		option_code_field_name	: cnty_code_field_name,
		option_label_field_name	: cnty_label_field_name, 
		callback_after_selected	: function(selected_cnty){
			Ti.API.debug('selected_cnty='+JSON.stringify(selected_cnty));
			if(selected_cnty){
				var selected_cnty_data = lookupCountryData(selected_cnty[cnty_code_field_name]);
				selected_cnty_trunk_code 	= selected_cnty_data[cnty_trunk_code_field_name];
				selected_phone_code			= selected_cnty_data[cnty_phone_code_field_name];
				Ti.API.debug('selected_cnty_trunk_code='+selected_cnty_trunk_code);
				Ti.API.debug('selected_phone_code='+selected_phone_code);
				$.country_code_label.text = selected_phone_code;
				hideValidationAlert();
			}else{
				selected_cnty_trunk_code	= null;
				selected_phone_code			= null;
				$.country_code_label.text 	= "";
			}		
		}
	});
	
	if(default_cnty_code){
		var default_cnty_data = lookupCountryData(default_cnty_code);
		selected_cnty_trunk_code 	= default_cnty_data[cnty_trunk_code_field_name];
		selected_phone_code			= default_cnty_data[cnty_phone_code_field_name];
		$.country_code_label.text 		= selected_phone_code;
	}
}

function lookupCountryData(selected_cnty_code){
	for(var i=0; i<cnty_data_source.length; i++){
		var cnty_data = cnty_data_source[i];
		if(cnty_data[cnty_code_field_name] == selected_cnty_code){
			return cnty_data;
		}
	}
	return null;
}

function initButton(){
	$.confirm_label.addEventListener('click', function(){
		if(checkInputValidation()){
			if(modal_window){
				if(callback_after_completed){
					var entered_phone_number = getEnteredMobilePhoneNumber();
					Ti.API.debug('>>>entered_phone_number='+entered_phone_number);
					callback_after_completed(entered_phone_number);
				}
				modal_window.close_modal();
			}
		}
	});
	$.mobile_number_input.addEventListener('change', function(){
		hideValidationAlert();
	});
}

function getEnteredMobilePhoneNumber(){
	var phone_number = $.mobile_number_input.value;
	if(phone_number){
		var trim_phone_number = phone_number.trim();
		if(selected_cnty_trunk_code){
			Ti.API.debug('selected_cnty_trunk_code='+selected_cnty_trunk_code);
			var entered_phone_trunk_code = trim_phone_number.substring(0, selected_cnty_trunk_code.length);
			Ti.API.debug('entered_phone_trunk_code='+entered_phone_trunk_code);
			if(selected_cnty_trunk_code==entered_phone_trunk_code){
				return selected_phone_code + trim_phone_number.substring(selected_cnty_trunk_code.length, trim_phone_number.length); 
			}else{
				return selected_phone_code + trim_phone_number;
			}
		}else{
			return selected_phone_code + trim_phone_number;
		}
		
	}else{
		return phone_number;
	}
}

function checkInputValidation(){
	if(selected_phone_code==null){
		showValidationAlert(L('pls.select.a.country'));
	}else{
		var phone_number = $.mobile_number_input.value;
		if(phone_number==null){
			showValidationAlert(L('pls.enter.mobile.phone.no'));
		}else{
			var trim_phone_number = phone_number.trim();
			if(trim_phone_number){
				hideValidationAlert();
				return true;
			}else{
				showValidationAlert(L('pls.enter.mobile.phone.no'));
			}
		}
	}
	return false;
}

function hideValidationAlert(){
	$.error_message_label.text = '';
	$.error_message_label.hide();
}

function showValidationAlert(alert_message){
	$.error_message_label.text = alert_message;
	$.error_message_label.show();
	
}
