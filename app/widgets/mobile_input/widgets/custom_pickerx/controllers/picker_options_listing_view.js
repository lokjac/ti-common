var args 							= arguments[0] || {};
var picker_modal_window				= args.picker_modal_window;
var picker_options_data_source 		= args.data_source;
var callback_after_selected			= args.callback_after_selected;
var option_code_field_name			= args.option_code_field_name;
var option_label_field_name			= args.option_label_field_name;
var selected_option_code			= args.selected_option_code;
var show_filter						= args.show_filter!=null? args.show_filter : true;
var $selected_option_view			= null;
var $selected_filtered_option_view	= null;
var selected_option_item			= null;
var selected_option_item_index		= null;
var SELECTED_OPTION_BGCOLOR			= '#EEEEEE';
var DEFAULT_OPTION_BGCOLOR			= '#FFFFFF';
var default_selected_option_item	= null;

initPickerOptionsListing();

function initPickerOptionsListing(){
	Ti.API.debug('---initPickerOptionsListing--- show_filter='+show_filter);
	if(picker_options_data_source){
		initPickerOptions();
	}
	initPickerOptionFilterInput();
}

function initPickerOptionFilterInput(){
	var show_filter_bool = show_filter === 'true' || show_filter;
	Ti.API.debug('show_filter_bool='+show_filter_bool);
	
	var searchbar_height = 44;
	if(Alloy.isTablet){
		searchbar_height = 50;
	}
	
	var searchbar = Titanium.UI.createSearchBar({
	    //barColor:'#555555',
	    backgroundColor: '#FFFFFF', 
	    showCancel:false,
	    height:searchbar_height,
	    hintTextColor: '#989898',
	    hintText: L('enter.to.filter'),
	    color: '#555555',
	    top:0,
	});
	
	if(show_filter_bool){
		$.picker_options_listing_full_listview.searchView = searchbar;
	}else{
		$.picker_options_listing_view.remove(searchbar);
		searchbar = null;
	}
	
	$.picker_option_cancel_label.addEventListener('click', function(){
		if(callback_after_selected){
			callback_after_selected(null);
			
		}
		if(picker_modal_window){
			picker_modal_window.close_modal();	
		}
	});
	
	$.picker_option_confirm_label.addEventListener('click', function(){
		if(callback_after_selected){
			callback_after_selected(selected_option_item.properties);
			
		}
		if(picker_modal_window){
			picker_modal_window.close_modal();	
		}
	});
	
}

function initPickerOptions(){
	var option_datas = [];
	var section = Ti.UI.createListSection();
	
	
	for(var i=0; i<picker_options_data_source.length; i++){
		var option_item = picker_options_data_source[i];
		var option_view = createOption(option_item);
		
		//$.picker_options_listing_full_listview.add(option_view);
		
		var option_code = option_item[option_code_field_name];
		var is_match = selected_option_code!=null && selected_option_code==option_code;
		
		//Ti.API.debug('is_match for '+option_code+" ="+is_match);
		if(is_match){
			//$selected_option_view 					= option_view.content_view;
			//$selected_option_view.backgroundColor 	= SELECTED_OPTION_BGCOLOR;
			selected_option_item					= option_item.properties;
			selected_option_item_index				= i;
		}
		
		option_datas.push(option_view);
		
	}
	section.setItems(option_datas);
	$.picker_options_listing_full_listview.sections = [section];
	
	$.picker_options_listing_full_listview.addEventListener('itemclick', function(e){
		var selected_option 	= e.section.getItemAt(e.itemIndex);
		Ti.API.debug('selected item index='+e.itemIndex);
		Ti.API.debug('selected_option='+JSON.stringify(selected_option.properties));
        //if(OS_ANDROID){
        	if(selected_option_item!=null){
				selected_option_item.properties.backgroundColor = DEFAULT_OPTION_BGCOLOR;
				section.updateItemAt(selected_option_item_index, selected_option_item);
			}
        	selected_option.properties.backgroundColor = SELECTED_OPTION_BGCOLOR;
        //}
        selected_option_item 		= selected_option;
        selected_option_item_index 	= e.itemIndex;
        
        section.updateItemAt(selected_option_item_index, selected_option);
 	});
	
	//showPickerFullOptions();
	$.picker_options_listing_full_listview.visible = true;
}

function createOption(option_item, is_filtered_option){
	var listitem_height = 38;
	if(Alloy.isTablet){
		listitem_height = 44;
	}
	var selected_backgroundcolor = SELECTED_OPTION_BGCOLOR;
	if(OS_ANDROID){
		selected_backgroundcolor = 'transparent';
	}
	var option_view = { 
				properties: {
            		itemId: option_item[option_code_field_name],
            		title: option_item[option_label_field_name],
            		//accessoryType: Ti.UI.LIST_ACCESSORY_TYPE_NONE,
            		accessoryType: Titanium.UI.LIST_ACCESSORY_TYPE_NONE,
            		color: '#555555',
            		searchableText: option_item[option_label_field_name],
            		selectedBackgroundColor: selected_backgroundcolor,
            		height: listitem_height,
            		backgroundColor: '#FFFFFF',
            		//touchEnabled: false
        		}
    };
    
    option_view[option_code_field_name] = option_item[option_code_field_name];
    option_view[option_label_field_name] = option_item[option_label_field_name];
    
    return option_view;
}
