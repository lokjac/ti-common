var args 							= arguments[0] || {};
var $picker_options_listing_view 	= null;
var $modal_window					= null;
var custom_picker_title				= null;
var WINDOW_CONTENT_TOP_PHONE		= 41;
var WINDOW_CONTENT_TOP_TABLET		= 50;
var OPTIONS_LISTING_VIEW_PATH		= "picker_options_listing_view";
var is_custom_picker_triggered		= false;
var config_options					= args;
var selected_option_code			= null;
var selected_option_label			= null;
var LOADING_PICKER_BG_COLOR			= "#A9A9A9";
var LOADING_PICKER_FONT_COLOR		= "#FFFFFF";
//var picker_data_source				= null;
var label_ori_color					= "#555555";
var label_ori_text					= null;

/*
 * options:
 * 	title -  Title of picker modal window
 * 	callback_after_selected - Callback function after selected picker option
 * 	callback_after_cancel - Callback function after canceled/closed the picker
 *  data_source - Data source to populate picker options
 * 	caller_window - Caller window reference object
 * 	option_label_field_name - Option item label field name in json
 * 	option_code_field_name - Option item code field name in json
 * 	show_filter - Show filter for custom picker,
 *  value - Default option value
 */
function init(options){
	Ti.API.debug('---CustomPicker.init---');
	initConfig(options);
	initPickerOptionModal();
	
}

function initConfig(options){
	Ti.API.debug('----custom_picker: initConfig----');
	if(options){
		Object.keys(options).forEach(function(key) {
			//Ti.API.debug('assigning config_option with key='+key);
			config_options[ key ] = options[ key ];
		}); 
	}
	
	if(config_options.text){
		$.custom_picker_label.text 		= config_options.text;	
		label_ori_text 					= config_options.text;
	}
	//Ti.API.debug('config_options.color='+config_options.color);
	if(config_options.color){
		//Ti.API.debug('assign color, where color='+config_options.color);
		$.custom_picker_label.color 	= config_options.color;
		label_ori_color 				= config_options.color;
	}else{
		$.custom_picker_label.color 	= label_ori_color;
	}
	if(config_options.top){
		$.custom_picker_view.top 	= config_options.top;
	}
	if(config_options.width){
		$.custom_picker_view.width 		= config_options.width;
	}
	if(config_options.show_filter){
		config_options['show_filter'] = config_options.show_filter;
	}
	if(config_options.text){
		$.custom_picker_label.text 		= config_options.text;	
		label_ori_text 					= config_options.text;
	}else{
		$.custom_picker_label.text 		= label_ori_text;
	}
	if(config_options.value){
		selected_option_code = config_options.value;
	}
	if(config_options.value){
		selected_option_code = config_options.value;
	}
	
	Ti.API.debug('initConfig: config_options.color='+config_options.color);
	Ti.API.debug('initConfig: label_ori_color='+label_ori_color);
	
	updateWithDefaultOptionCode(
		config_options.value,
		config_options.data_source,
		config_options.matching_strategy,
		callback_after_selected
	);
}

function update(options){
	Ti.API.debug('---CustomPicker.update---');
	initConfig(options);
	
	//picker_data_source 		= config_options.data_source;
	//Ti.API.debug('update: picker_data_source='+JSON.stringify(picker_data_source));
	
}

function callback_after_selected(option_item){
	Ti.API.debug('------------------- callback_after_selected --------------------');
		if(option_item){
			Ti.API.debug('option_item='+JSON.stringify(option_item));
			
			var _selected_option_code 		= option_item.itemId;
			var _selected_option_label 		= option_item.title;
			
			//var _selected_option_code 		= option_item[config_options.option_code_field_name];
			//var _selected_option_label 		= option_item[config_options.option_label_field_name];
			
			selected_option_code 			= _selected_option_code;
			selected_option_label 			= _selected_option_label;
			$.custom_picker_label.text 		= _selected_option_label;
			$.custom_picker_label.color		= label_ori_color;
			
			Ti.API.debug('_selected_option_code='+_selected_option_code);
			Ti.API.debug('_selected_option_label='+_selected_option_label);
			Ti.API.debug('label_ori_color='+label_ori_color);
			
			$.custom_picker_label.text = selected_option_label;
				
		}else{
			$.custom_picker_label.text = label_ori_text;
			selected_option_code 	   = null;
			selected_option_label      = null;
		}
		if(config_options.callback_after_selected){
			var option_properties = null;
			if(option_item){
				option_properties = {};
				option_properties[config_options.option_code_field_name] = option_item.itemId;
				option_properties[config_options.option_label_field_name] = option_item.title;
			}else{
				
			}
			config_options.callback_after_selected(option_properties);
		}
}

function updateWithDefaultOptionCode(default_option_code, option_data_source, matching_strategy, callback_after_selected){
	if(matching_strategy==null){
		matching_strategy = function(checking_option, default_option_code){
			var checking_option_code 	= checking_option[config_options.option_code_field_name];
			var checking_option_label 	= checking_option[config_options.option_label_field_name];
			Ti.API.debug('default matching strategy: comparing '+default_option_code+' and '+checking_option_code); 
			if(checking_option_code == default_option_code){
				return true;
			}
			return false;
		};
	}
	if(matching_strategy && option_data_source){
		for(var i=0; i<option_data_source.length; i++){
			var checking_option = option_data_source[i];
			if(matching_strategy(checking_option, default_option_code)){
				selected_option_label = checking_option[config_options.option_label_field_name];
				$.custom_picker_label.text = selected_option_label;
				//callback_after_selected(checking_option);	
				break;
			}
		}
	}
}

function initModalWindow(){
	
	var callback_after_closed = function(){
		Ti.API.debug('callback after closed');
		if(config_options.callback_after_cancel){
			config_options.callback_after_cancel();
		}
	};
	var callback_after_opened = function(){
		Ti.API.debug('callback after opened');
		is_custom_picker_triggered = false;
		$.custom_picker_view.backgroundColor 	= '#FFFFFF';
		$.custom_picker_label.color 			= label_ori_color;
		if(selected_option_code==null){
			$.custom_picker_label.text				= label_ori_text;	
		}else{
			$.custom_picker_label.text				= selected_option_label;
		}
		
	};
	$modal_window = createPopupModalViewWindow(OPTIONS_LISTING_VIEW_PATH, 
											config_options.title, 
											{
												data_source				: config_options.data_source,
												callback_after_selected	: callback_after_selected,
												option_code_field_name	: config_options.option_code_field_name,
												option_label_field_name	: config_options.option_label_field_name,
												selected_option_code	: selected_option_code,
												show_filter				: config_options.show_filter,
											},
											callback_after_closed, 
											callback_after_opened
											);
}

function getValue(){
	return selected_option_code;
}

function initPickerOptionModal(){
	$.custom_picker_view.addEventListener('click', function(){
		$.custom_picker_view.backgroundColor 	= LOADING_PICKER_BG_COLOR;
		$.custom_picker_label.color 			= LOADING_PICKER_FONT_COLOR;
		$.custom_picker_label.text				= L('loading');
		if(is_custom_picker_triggered==false){
			initModalWindow();
			is_custom_picker_triggered = true;
		}
	});
}

function createPopupModalViewWindow(view_path, title, params, callback_after_closed, callback_after_opened){
	var modalWindow = Ti.UI.createWindow({
						modal:true, 
						backgroundColor: "#FFFFFF", 
						layout:"absolute",
						height: Ti.UI.FILL,
						width: Ti.UI.FILL,
						fullscreen: true,
						navBarHidden: true
					});
	if(OS_ANDROID){
		modalWindow.windowSoftInputMode = Titanium.UI.Android.SOFT_INPUT_STATE_HIDDEN;
	}
	
	var navigation_view = Ti.UI.createView({
		width	:Ti.UI.FILL,
	    layout	: 'vertical',
	    height	: Ti.UI.SIZE,
	    top: 0,
	});
	
	var close_modal_function = null;
	if(params && params['close_modal_function']){
		close_modal_function = params['close_modal_function'];
	}
	
	var close_modal = function(param){
		Ti.API.debug('---start closing modal window---');
		Ti.API.debug('param='+JSON.stringify(param));
		Ti.API.debug('---end closing modal window---');
		if(modalWindow){
			modalWindow.close(param);
			modalWindow = null;
			//modalWindow.visible = false;
			Ti.API.debug('Modal window is closed properly');
		}
		Ti.API.debug('selected_option_code after closed modal window='+selected_option_code);
		//callback_after_selected(selected_option_code);
	};
	modalWindow.addEventListener('open', function(param){
		Ti.API.debug('Modal window is opened');
		if(callback_after_opened){
			callback_after_opened();
		}				
	});
	
	modalWindow.addEventListener('close', function(param){
		Ti.API.debug('close modal window');
		if(callback_after_closed && (modalWindow==null || modalWindow.ignore_close_callback==false)){
			callback_after_closed(param);
			Ti.API.debug('call callback after closed');
		}else{
			Ti.API.debug('ignore to call callback after closed');
		}				
	});
	
	var navigation_bar = createNavigationBar(function(){
			if(close_modal_function){
				close_modal_function();
			}else{
				close_modal();
			}
		},
		title
	);
	
	//navigation_view.add(navigation_view);
	modalWindow.add(navigation_bar.getView());
	
	var navigation_title = navigation_bar.navigation_title;
	
	var view_params = {};
	if(params){
		view_params = params;	
	}
	view_params['picker_modal_window'] = modalWindow;
	
	Ti.API.debug('params='+JSON.stringify(params));
	
	view_params['modalWindow'] 		= modalWindow;
	view_params['navigation_bar'] 	= navigation_bar;
	view_params['navigation_title'] = navigation_title;
	
	var content_view = Widget.createController(view_path, view_params).getView();
	Ti.API.debug('content_view='+content_view);
	if(Alloy.isTablet){
		content_view.setTop(WINDOW_CONTENT_TOP_TABLET);	
	}else{
		content_view.setTop(WINDOW_CONTENT_TOP_PHONE);
	}
			
	content_view.window 	= modalWindow;
	modalWindow.close_modal	= close_modal;
	
	modalWindow.add(navigation_view);
	modalWindow.add(content_view);
	modalWindow.open();
	return modalWindow;
}

function createNavigationBar(back_button_callback, title){
	var navigation_bar = Widget.createController("navigation_bar",{
		title: title,
		back_button_callback: back_button_callback
	});
	return navigation_bar;
}

function hide(){
	$.custom_picker_view.hide();
	$.custom_picker_view.top    = 0;
	$.custom_picker_view.height = 0;
}

function show(){
	$.custom_picker_view.show();
	if(Alloy.isTablet){
		$.custom_picker_view.top    = 10;
		$.custom_picker_view.height = 44;
	}else{
		$.custom_picker_view.top    = 5;
		$.custom_picker_view.height = 38;
	}
}

exports.init 	= init;
exports.update	= update;
exports.value	= getValue;
exports.hide	= hide;
exports.show	= show;
