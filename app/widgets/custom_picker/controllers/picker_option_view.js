var args 					= arguments[0] || {};
var option_label 			= args.option_label;
var option_code 			= args.option_code;
var option_value 			= args.option_value;
var window					= args.window;
var action					= args.action;
var option_value_element	= args.option_value_element;
var show_separator			= args.show_separator!=null? args.show_separator : true;

initPickerOption();

function initPickerOption(){
	initPickerOptionLabel();
	initPickerOptionValueElement();
	initPickerOptionAction();
	initSeparator();
	$.picker_option_view.content_view 				= $.picker_option_content_view;
	$.picker_option_view.content_view.option_code 	= option_code;
}

function initPickerOptionLabel(){
	if(option_label){
		$.picker_option_label.text = option_label;
	}
	
	if(option_value){
		$.picker_option_value_label.text = option_value;
	}
}

function initPickerOptionAction(){
	
	if(action){
		if(option_value_element){
			$.picker_option_content_view.addEventListener('click', function(){
				action($.picker_option_content_view);	
			});
		}else{
			$.picker_option_content_view.addEventListener('click', function(){
				action($.picker_option_content_view);	
			});	
		}
		
		
	}else{
		$.picker_option_content_view.remove($.picker_option_action_view);
		$.picker_option_action_view = null;
		$.picker_option_value_view.right = 10;
	}
	
}


function initPickerOptionValueElement(){
	if(option_value_element){
		$.picker_option_value_view.remove($.picker_option_value_label);
		$.picker_option_value_label = null;
		$.picker_option_value_view.add(option_value_element);
		Ti.API.debug('Created option value element='+option_value_element);
	}
}

function initSeparator(){
	if(show_separator == false){
		$.picker_option_view.remove($.picker_option_separator_view);
		$.picker_option_separator_view = null;
	}
}
