
var args 					= arguments[0] || {};
var title					= args.title;
var back_button_callback	= args.back_button_callback;

initNavigationBar();

function initNavigationBar(){
	if(back_button_callback && $.back_btn){
		$.back_btn.addEventListener('click', function(event){
			Ti.API.debug('Clicked navigation back button');
			if(back_button_callback){
				back_button_callback();
			}
		});
	}
	if(title){
		$.navigation_title.text = title;
	}
	
}
exports.back_btn 			= $.back_btn;
exports.navigation_title 	= $.navigation_title;
