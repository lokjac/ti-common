var args 							= arguments[0] || {};
var picker_modal_window				= args.picker_modal_window;
var picker_options_data_source 		= args.data_source;
var callback_after_selected			= args.callback_after_selected;
var option_code_field_name			= args.option_code_field_name;
var option_label_field_name			= args.option_label_field_name;
var selected_option_code			= args.selected_option_code;
var show_filter						= args.show_filter!=null? args.show_filter : true;
var $selected_option_view			= null;
var $selected_filtered_option_view	= null;
var selected_option_item			= null;
var SELECTED_OPTION_BGCOLOR			= '#EEEEEE';
var DEFAULT_OPTION_BGCOLOR			= '#FFFFFF';

initPickerOptionsListing();

function initPickerOptionsListing(){
	Ti.API.debug('---initPickerOptionsListing--- show_filter='+show_filter);
	if(picker_options_data_source){
		initPickerOptions();
	}
	initPickerOptionFilterInput();
}

function initPickerOptionFilterInput(){
	var show_filter_bool = show_filter === 'true';
	Ti.API.debug('show_filter_bool='+show_filter_bool);
	if(show_filter_bool){
		$.picker_options_filter_input.addEventListener('change', function(){
			var enter_value = $.picker_options_filter_input.value;
			if(enter_value){
				$.picker_option_filter_cancel_button.visible = true;
				$.picker_option_filter_button.visible = true;
			}else{
				$.picker_option_filter_cancel_button.visible = false;
				$.picker_option_filter_button.visible = false;
			}
		});
		
		$.picker_option_filter_cancel_button.addEventListener('click', function(){
			$.picker_options_filter_input.value = "";
			$.picker_options_filter_input.blur();
			$.picker_option_filter_cancel_button.visible = false;
			$.picker_option_filter_button.visible = false;
			showPickerFullOptions();
		});
		
		$.picker_option_filter_button.addEventListener('click', function(){
			var enter_value = $.picker_options_filter_input.value;
			if(enter_value){
				$.picker_options_filter_input.blur();
				filterPickerOptions(enter_value);
				$.picker_option_filter_button.visible = true;	
			}
		});
	}else{
		$.picker_options_listing_view.remove($.picker_options_filter_view);
		$.picker_options_listing_view.remove($.picker_option_filter_separator_view);
		
	}
	
	$.picker_option_cancel_label.addEventListener('click', function(){
		if(callback_after_selected){
			callback_after_selected(null);
			
		}
		if(picker_modal_window){
			picker_modal_window.close_modal();	
		}
	});
	
	$.picker_option_confirm_label.addEventListener('click', function(){
		if(callback_after_selected){
			callback_after_selected(selected_option_item);
			
		}
		if(picker_modal_window){
			picker_modal_window.close_modal();	
		}
	});
}

function initPickerOptions(){
	for(var i=0; i<picker_options_data_source.length; i++){
		var option_item = picker_options_data_source[i];
		var option_view = createOption(option_item);
		$.picker_options_listing_full_scrollview.add(option_view);
		var option_code = option_item[option_code_field_name];
		var is_match = selected_option_code!=null && selected_option_code==option_code;
		//Ti.API.debug('is_match for '+option_code+" ="+is_match);
		if(is_match){
			$selected_option_view 					= option_view.content_view;
			$selected_option_view.backgroundColor 	= SELECTED_OPTION_BGCOLOR;
			selected_option_item 					= option_item;
		}
	}
	showPickerFullOptions();
}

function showPickerFullOptions(){
	$.picker_options_listing_full_scrollview.visible = true;
	$.picker_options_listing_filtered_scrollview.visible = false;
	if(selected_option_item!=null){
		var __selected_option_code = selected_option_item[option_code_field_name];
		if($selected_option_view.option_code!=__selected_option_code){
			Ti.API.debug('Have to change show selected option for full options listing');
			var option_views = $.picker_options_listing_full_scrollview.children;
			for(var i=0; i<option_views.length; i++){
				var view = option_views[i];
				Ti.API.debug('Option code ='+view.content_view.option_code);
				if(__selected_option_code==view.content_view.option_code){
					$selected_option_view.backgroundColor 	= DEFAULT_OPTION_BGCOLOR;
					$selected_option_view 					= view.content_view;
					$selected_option_view.backgroundColor	= SELECTED_OPTION_BGCOLOR;
					
					break;
				}
			}
		}
	}
}

function filterPickerOptions(filter_value){
	//Ti.API.debug('filter_value='+filter_value);
	$.picker_options_listing_filtered_scrollview.removeAllChildren();
	
	var v_regexp = new RegExp(filter_value,"i");
	
	for(var i=0; i<picker_options_data_source.length; i++){
		var option_item 	= picker_options_data_source[i];
		var option_code 	= option_item[option_code_field_name];
		var option_label 	= option_item[option_label_field_name];
		var is_match 		= v_regexp.test(option_label);
		
		if(is_match){
			var option_view = createOption(option_item, true);
			$.picker_options_listing_filtered_scrollview.add(option_view);	
			if(selected_option_code!=null && option_code==selected_option_code){
				$selected_filtered_option_view 				= option_view.content_view;
				option_view.content_view.backgroundColor 	= SELECTED_OPTION_BGCOLOR;
			}
		}
	}
	$.picker_options_listing_full_scrollview.visible = false;
	$.picker_options_listing_filtered_scrollview.visible = true;
	
}

function createOption(option_item, is_filtered_option){
	var selected_callback = function(selected_option_view){
								Ti.API.debug('selected '+option_item[option_code_field_name]);
								if(is_filtered_option){
									if($selected_filtered_option_view){
										$selected_filtered_option_view.backgroundColor = DEFAULT_OPTION_BGCOLOR;	
									}
									$selected_filtered_option_view 					= selected_option_view;
									$selected_filtered_option_view.backgroundColor 	= SELECTED_OPTION_BGCOLOR;
									
								}else{
									if($selected_option_view){
										$selected_option_view.backgroundColor = DEFAULT_OPTION_BGCOLOR;	
									}
									$selected_option_view 					= selected_option_view;
									$selected_option_view.backgroundColor 	= SELECTED_OPTION_BGCOLOR;
								}
								selected_option_item 					= option_item;
								Ti.API.debug('selected_option_item='+JSON.stringify(selected_option_item));
							};
	
	var option_view = Widget.createController('picker_option_view', {
					option_label		: option_item[option_label_field_name],
					option_code			: option_item[option_code_field_name],
					action				: selected_callback
	}).getView();
	return option_view;
}
