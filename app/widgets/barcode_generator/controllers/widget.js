var args				= arguments[0] || {};

var BARCODE_HTML_PATH 	= '/barcode_generator/barcode.html';

var createBarCodeView = function(code, options) {
	Ti.API.debug('---createBarCodeView---, code='+code);
	Ti.API.debug('options b4='+JSON.stringify(options));
	var barcode_html_path = BARCODE_HTML_PATH;
	
	if (options === undefined) {
		options = {
			height: args.height || 30,
			width: 240,
			url: barcode_html_path
		};
	}
	else {
		if (options.height === undefined)  options.height = args.height || 30;
		if (options.width === undefined) options.width = 240;
		if (options.url === undefined) options.url = barcode_html_path;
	}
	
	Ti.API.debug('options after='+JSON.stringify(options));
	
	var self = Ti.UI.createWebView(options);
	
	self.addEventListener("touchmove", function(e){
	    e.preventDefault();
	});
	
	Ti.App.addEventListener('generateBarcode', function(){
		self.evalJS("doBarcode(unescape('" + code + "'), "+options.height+")");
		var barcode_code = self.evalJS("getBarcode()");
		Ti.API.debug('barcode_code='+barcode_code);
		$.barcode_label.text = barcode_code;
		
	});
	
	return self;
};

function init(code, options){
	$.bar_code_view.add(createBarCodeView(code, options));
}

exports.init = init;