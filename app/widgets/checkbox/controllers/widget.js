var args = arguments[0] || {};

var _value = false;

function setValue(val) {
	_value = val;
	updateDisplay();
}


function getValue() {
	return _value;
}

function updateDisplay() {
	var check_value = getValue();
	if(check_value){
		$.chk.text = '\u2713';
	}else{
		$.chk.text = '';
	}
}

function setVisible(visibility){
	$.checkbox.visible = visibility;

}

exports.init = function (callback) {
	$.lbl.text = args.message || 'Set "message" attribute to change';
	if(args.color){
		$.lbl.setColor(args.color);
		$.chk.setColor(args.color);	
	}
	
	if(args.boxHeight){
		$.chk.setHeight(args.boxHeight);	
	}
	
	if(args.boxWidth){
		$.chk.setWidth(args.boxWidth);	
	}
	
	if(args.labelFontSize){
		$.lbl.setFont({
			fontSize: args.labelFontSize,
		});	
	}
	
	if(args.boxFontSize){
		$.chk.setFont({
			fontSize: args.fontSize,
		});	
	}
	
	if(args.boxLeft){
		//Ti.API.debug('set box left='+args.boxLeft);
		$.chk.setLeft(args.boxLeft);	
	}
	
	if(args.labelLeft){
		//Ti.API.debug('set label left='+args.labelLeft);
		$.lbl.setLeft(args.labelLeft);	
	}
	
	
	$.checkbox.addEventListener('click', function() {
		var checkState = !_value;
		Ti.API.info('checkState='+checkState);
		setValue(checkState);
		callback(checkState);
	});
	_.extend($.chk, args);
	_.extend($.lbl, args);
};

exports.value			= getValue;
exports.setValue		= setValue;
exports.setVisible		= setVisible;
