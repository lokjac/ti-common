var args							= arguments[0] || {};
var config_options					= args;
/*
 * 	options param:
 * 		page_view_paths - Page view path(s)
 * 		page_control_margin - Paginator control margin. Default value is 15.
 * 		page_control_size - Paginator control size. Default value is 8.
 * 		pageColor - Paginator control color. Default value is #555555
 * 		caller_window - Caller window.
 * 
 */
function init(options){
	initConfig(options);
}

function initConfig(options){
	Ti.API.debug('----multi_views: initConfig----');
	if(options){
		Object.keys(options).forEach(function(key) {
			config_options[ key ] = options[ key ];
		}); 
	}
	
	if(config_options.width){
		$.multi_views_content_view.width 		= config_options.width;
	}
	if(config_options.height){
		$.multi_views_content_view.height 		= config_options.height;
	}
	if(config_options.top){
		$.mobile_input_view.top 				= config_options.top;
	}
	if(config_options.bottom){
		$.mobile_input_view.bottom 				= config_options.bottom;
	}
	if(config_options.left){
		$.mobile_input_view.left 				= config_options.left;
	}
	if(config_options.right){
		$.mobile_input_view.right 				= config_options.right;
	}
	
	Ti.API.debug('config_options='+JSON.stringify(config_options));
	
	createPageViews(config_options.page_view_paths);
	createScrollableViewPagingControl(config_options);
}

function createPageViews(page_view_paths){
	if(page_view_paths){
		var views = [];
		for(var i=0; i<page_view_paths.length; i++){
			var view_controller = Alloy.createController(page_view_paths[i], {
				caller_window: config_options.caller_window
			});
			views.push(view_controller.getView());
			//$.multi_views_container.add(view_controller.getView());
		}
		$.multi_views_container.views = views;
	}
}

function createScrollableViewPagingControl(args){
	
	var scrollableView 		= $.multi_views_container;
	var page_views			= scrollableView.getViews();
	var page_control_margin = args.page_control_margin || 15;
	var page_control_size	= args.page_control_size || 8;
	var pageColor 			= args.page_color || '#555555';
	var callback			= args.callback;
	
	Ti.API.debug('>>>page_control_margin='+page_control_margin);
	Ti.API.debug('>>>page_control_size='+page_control_size);
	Ti.API.debug('>>>scrollableView='+scrollableView);
	Ti.API.debug('>>>page_views='+page_views);
	
	var page_control_top				= 2;
	var page_control_container_height 	= page_control_size + page_control_top;
	
	if(scrollableView && page_views){
		var container = $.multi_views_paginator_view;
		
		var border_radius = Math.round(page_control_size/2);
		
		var number_of_pages = page_views.length;//scrollableView.getViews().length;
		var pages = []; // without this, the current page won't work on future references of the module 
		
		Ti.API.debug('number_of_pages='+number_of_pages);
		
		for (var i = 0; i < number_of_pages; i++) {
			var page = Titanium.UI.createView({
				borderRadius	: border_radius,
				width			: page_control_size,
				height			: page_control_size,
				left			: page_control_margin,
				backgroundColor	: pageColor,
				top 			: page_control_top,
				opacity			: 0.5
			});
			// Store a reference to this view
			pages.push(page);
			// Add it to the container
			container.add(page);
			page.index = i;
			page.addEventListener('click', function(){
				scrollableView.scrollToView(this.index);
			});
			
		}
		
		pages[0].setOpacity(1);
		
		// Callbacks
		var onScroll = function(event){
			// Go through each and reset it's opacity
			for (var i = 0; i < number_of_pages; i++) {
				pages[i].setOpacity(0.5);
			}
			// Bump the opacity of the new current page
			pages[event.currentPage].setOpacity(1);
			//Ti.API.debug('scroll to page '+event.currentPage);
			if(callback){
				var scrolled_view_index = event.getCurrentPage();
				var scrollable_views 	= scrollableView.getViews();
				var scrolled_view		= scrollable_views[scrolled_view_index];
				callback(scrolled_view);
			}
			
		};
		
		// Attach the scroll event to this scrollableView, so we know when to update things
		scrollableView.addEventListener("scrollend", onScroll);
		
		return container;
	}else{
		return null;
	}
	
}

exports.init		= init;